#include <iostream>
#include <cstdio>
#include <map>
#include <cmath>
#include <fstream>
#include <vector>
#include <functional>

using namespace std;

// Pi zdefiniowane do 20 miejsca po przecinku
#define M_PI 3.14159265358979323846
#define M_E	2.71828182846


vector<double> variables; //  tablica wsp�lczynnik�w przy wielomianie

// Obliczanie ca�ki z fx metod� trapez�w
double integral_Trapeze2(function<double(double)> fx, double section_start = 0, double section_end = M_PI, double variation = pow(10, -4))
{
	int step = 100;		// krok dla kolejnych punkt�w meody trapez�w
	int loopcount = 0;	// licznik wykonania p�tli
	double previous_result = 0;	// poprzedni wynik przyblizenia
	double result = -100;	// obecny wynik przybli�enia
	double deltaX = 0, integral = 0;
	int stepchange=1;
	double start;
	int counted_points = 2;
	integral = (fx(section_start) / 2) + (fx(section_end) / 2); // dla metody trapez�w - pierwsza i ostatnia warto�c
	while (abs(previous_result - result) > variation)
	{
		
		loopcount++;
		previous_result = result;
		if (loopcount > 100) break;

		deltaX = (section_end - section_start) / step;
		start = section_start;

		for (int i = 1; i < step - 1; i++)
		{
			start += deltaX;
			if (i%stepchange == 0 || step==100)
			{
				integral = integral + fx(start);
				counted_points++;
			}
		}

		result = integral* deltaX;
		cout << result << " |\t " << abs(previous_result - result) << " |\t " << step << " |\t " << counted_points << " | \t\n";
		
		step *= 2;
		stepchange *= 2;
		
		
	}
	
	return result;
}

// Obliczanie ca�ki z fx metod� trapez�w
double integral_Trapeze(function<double(double)> fx, int step = 100000, double section_start = 0, double section_end = M_PI)
{
	double deltaX = (section_end - section_start) / step;

	double integral = (fx(section_start) / 2) + (fx(section_end) / 2);
	for (int i = 1; i < step - 1; i++) integral = integral + fx(section_start += deltaX);
	integral = integral* deltaX;
	//	cout << "(Dla " << step << " punktow) ";
	return integral;
}

// Obliczanie ca�ki z fx przy uzyciu kwadratury typu Gaussa
double integral_gauss(function<double(double)> fx, double section_start = 0, double section_end = M_PI)
{
	double x1 = 0.5 * (section_start + section_end);
	double x2 = 0.5 * (section_end - section_start);
	double integral = 0;
	double x[] = { 0.1488743389, 0.4333953941, 0.6794095682, 0.8650633666, 0.9739065285 }; 		// Wartosci i wagi pobrane z podrecznika:
	double weight[] = { 0.2955242247, 0.2692667193, 0.2190863625, 0.1494513491, 0.0666713443 };	// "Numerical Recipes in C" W.H. Press'a
	int size = sizeof(weight) / sizeof(*weight);
	double deltax = 0;
	for (int i = 0; i <size; i++)
	{
		deltax = x2*x[i];
		integral += weight[i] * (fx(x1 + deltax) + fx(x1 - deltax));
	}
	integral *= x2;
	return integral;
}


double tanh_fx(double var, function<double(double)> fx)
{
	double temp = pow(M_E, -var);
	double var2 = pow(M_E, var - temp);
	double result = fx(var2) * var2*(1 + pow(M_E, -var));
	return result;
}

// Obliczanie ca�ki z fx przy uzyciu kwadratury typu tanh-sinh
double integral_tanh_sinh(function<double(double)> fx, int step = 10000, double section_start =-3, double section_end = 3)
{
	double deltaX = (abs(section_end - section_start)) / step;

	double integral = (tanh_fx(section_start,fx) / 2) + (tanh_fx(section_end,fx) / 2);
	for (int i = 1; i < step - 1; i++) integral = integral + tanh_fx(section_start += deltaX,fx);
	integral = integral*deltaX;
	return integral;

}


// funkcja w postaci wielomianu
double polyFunc(double var)
{
	double func = 0.0;
	int power = 0;
	for each (double v in variables)
	{
		func += v*pow(var, power);
		power++;
	}

	return func;
}

double funcX(double var)
{
	return (polyFunc(var)*pow(M_E, -var)) / sqrt(var);
}


int main()
{
	variables.assign({ 1, 2, 3,4 });

	double start_point =  pow(2,-4);
	double end_point = pow(2, 4);

	function<double(double)> func = funcX;
	cout << "Trapez (Zwykly):      \t\t" << integral_Trapeze(func,100000,start_point,end_point) << endl;
//	cout << "Trapez (Strojacy):    \t\t" << integral_Trapeze2(func) << endl;
	cout << "Kwadratura Gauss'a:   \t\t" << integral_gauss(func, start_point, end_point) << endl;
	cout << "Kwadratura tanh-sinh: \t\t" << integral_tanh_sinh(func) << endl;
	
	
	
	system("PAUSE");
	return 0;
}