#include <iostream>
#include <cstdio>
#include <map>
#include <cmath>
#include <fstream>
#include <vector>
#include <functional>

using namespace std;

// Pi zdefiniowane do 20 miejsca po przecinku
#define M_PI 3.14159265358979323846


vector<double> variables; //  tablica wsp�lczynnik�w przy wielomianie

//--------------------------------
// Sinus rozwini�ciem Tylora

//Liczenie silni
double fact(double factor)
{
	double var = 1.0;
	for (int i = factor; i > 1; i--)
	{
		var *= i;
	}
	return var;
}

// Interpolacja przy u�yciu rozwini�cia Tylora
double sin_tylor(double var, double accur = 10)
{
	double sin = 0.0;
	for (int i = 0; i < accur; i++)
	{
		sin = sin + ((1 / fact(2 * i + 1)) * pow(var, 2 * i + 1) * pow(-1, i));
	}
	return sin;

}

//-------------------------------- 

// metoda obliczaj�ca funkcj� sin(x)*f(x) w punkcie var
double sin_fx(double var, function<double(double)> fx)
{
	return sin_tylor(var)* fx(var);

}


// Obliczanie ca�ki z fx metod� trapez�w
double integral_Trapeze(function<double(double)> fx, int step = 10000, double section_start =0, double section_end = M_PI)
{
	double deltaX = (section_end - section_start) / step;

	double integral = (sin_fx(section_start,fx) / 2) + (sin_fx(section_end,fx)/2);
	for (int i = 1; i < step - 1; i++) integral = integral + sin_fx(section_start += deltaX,fx);
	integral = integral* deltaX;
//	cout << "(Dla " << step << " punktow) ";
	return integral;
}

// Obliczanie ca�ki z fx metod� Simpsona
double integral_Simpson(function<double(double)> fx, int step = 10000, double section_start = 0, double section_end = M_PI)
{
	double deltaX = (section_end - section_start) / step;
	double integral = (sin_fx(section_start, fx)*(deltaX / 3)) + (sin_fx(section_end, fx) / 2);
	for (int i = 1; i < step - 1; i++)
	{
		if (i % 2 == 0) integral = integral + (deltaX / 3) * 2 * sin_fx(section_start += deltaX, fx);
		else integral = integral + (deltaX / 3) * 4 * sin_fx(section_start += deltaX, fx);
	}
//	cout << "(Dla " << step << " punktow) ";
	return integral;
}

// Obliczanie ca�ki z fx metod� trapez�w z zag�szczaniem rekurencyjnym
double recursion_Trapeze(function<double(double)> fx,double variation = pow(10,-4), int basic_step_threshold = 10, double privious_result=0,int stepcount=0)
{
	stepcount++;
	
	double result = integral_Trapeze(fx,basic_step_threshold);
	if (stepcount > 10000) return result;
	if (abs(privious_result - result) <= variation)
	{
		cout << "(Dla " << basic_step_threshold << " punktow) ";
		return result;
	}
	else return recursion_Trapeze(fx, variation, 2*basic_step_threshold, result,stepcount);

}

// Obliczanie ca�ki z fx metod� simpsona z zag�szczaniem rekurencyjnym
double recursion_Simpson(function<double(double)> fx, double variation = pow(10, -4), int basic_step_threshold = 10, double privious_result = 0, int stepcount = 0)
{
	stepcount++;

	double result = integral_Simpson(fx, basic_step_threshold);
	if (stepcount > 10000) return result;
	if (abs(privious_result - result) <= variation)
	{
		cout << "(Dla " << basic_step_threshold << " punktow) ";
		return result;
	}
	else return recursion_Simpson(fx, variation, 2 * basic_step_threshold, result, stepcount);

}

// Obliczanie ca�ki z fx przy uzyciu kwadratury typu Gaussa
double integral_gauss(function<double(double)> fx, double section_start = 0, double section_end = M_PI)
{
	double x1 = 0.5 * (section_start + section_end);
	double x2 = 0.5 * (section_end - section_start);
	double integral = 0;
	double x[] = {0.1488743389, 0.4333953941, 0.6794095682, 0.8650633666, 0.9739065285 }; 		// Wartosci i wagi pobrane z podrecznika:
	double weight[] = {0.2955242247, 0.2692667193, 0.2190863625, 0.1494513491, 0.0666713443 };	// "Numerical Recipes in C" W.H. Press'a
	int size = sizeof(weight) / sizeof(*weight);
	double deltax = 0;
	for (int i = 0; i <size; i++)
	{
		deltax = x2*x[i];
		integral += weight[i]* (sin_fx(x1 + deltax, fx) + sin_fx(x1 - deltax, fx));
	}
	integral *= x2;
	return integral;
}

double polyFunc(double var)
{
	double func = 0.0;
	int power = 0;
	for each (double v in variables)
	{
		func += v*pow(var, power);
		power++;
	}

	return func;
}

int main()
{
	variables.assign({ 1, 2, 3,4});

	function<double(double)> func = polyFunc;
	cout << "Trapez (Zwykly):    \t\t"<< integral_Trapeze(func)<<endl;
	cout << "Simpson(Zwykly):    \t\t" << integral_Simpson(func) << endl;
	cout << "Trapez (Rekurencja):\t\t " << recursion_Trapeze(func) << endl;
	cout << "Simpson(Rekurencja):\t\t" << recursion_Simpson(func) << endl;
	cout << "Kwadratura Gauss'a: \t\t" << integral_gauss(func) << endl;

	system("PAUSE");
	return 0;
}