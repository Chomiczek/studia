#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <chrono>
#include <fstream>
#include "Eigen\Eigen"


using namespace std;
using namespace Eigen;

fstream file;


// konwertowanie complex<couble> na double
double Complex2Double(complex<double> val)
{
	return sqrt(val.real()*val.real() + val.imag()*val.imag());

}

double get_random(double min, double max)
{
	double result = min - 1;
	double before;
	double after;
	while (result < min || result > max)
	{
		before = rand() % (int)max + (int)min;
		after = (double)rand() / RAND_MAX;
		result = before + after;
	}
	
	return result;
}

VectorXd random_B(double n)
{
	VectorXd b = VectorXd::Zero(n);
	for (int i = 0; i < n; i++)
	{
		b(i) = get_random(0, 3.14);
	}
	return b;
}

MatrixXd Generate_Matrix(int num)
{
	int n = (num - 2)*(num - 2);
	MatrixXd m = MatrixXd::Zero(n, n);
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
		{
			if (i == j)
				m(i - 1,j - 1) = 4.0;
			else if ((i == (j + 1)) && ((i - 1) % (num - 2) != 0))
				m(i - 1,j - 1) = -1.0;
			else if ((i == (j - 1)) && ((j - 1) % (num - 2) != 0))
				m(i - 1,j - 1) = -1.0;
			else if ((i == (j - num + 2)) || (i == (j + num - 2)))
				m(i - 1,j - 1) = -1.0;
			else
				m(i - 1,j - 1) = 0.0;
		}
	return m;

}

// Liczy ilosc zer w macierzy
int Count_Zeros(MatrixXd matrix)
{
	int counter = 0;
	int size = sqrt(matrix.size());
	for (int i = 0; i < size ; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (matrix(i, j) == 0) counter++;
		}
	}
	return counter;

}


// Oblicza wsp�czynnik uwarunkowania
double Con_Factor(MatrixXd matrix)
{
	MatrixXcd eg = matrix.eigenvalues();
	double max = 0.0;
	double min = Complex2Double(eg(0,0));
	int size = eg.size();
	double temp;
	for(int i=0;i<size;i++)
	{
		temp = Complex2Double(eg(i, 0));
		if (temp > max) max = temp;
		else if (min > temp) min = temp;
	}

	return max/min;
}


// Tworzenie macierzy LU (implementacja w�asna)
MatrixXd Get_LU_Matrix(MatrixXd matrix)
{
	MatrixXd LU = matrix;

	int size = sqrt(matrix.size());
	int sum = 0;
	for (int i = 0; i < size; i++) {


		for (int k = i; k < size; k++) 
		{
			sum = 0;
			for (int j = 0; j < i; j++) 
				sum += (LU(i, j) * LU(j, k));
			LU(i, k) = matrix(i, k) - sum;

		}


		for (int k = i; k < size; k++)  
		{
			if (i == k) continue; 
			else {

				sum = 0;
				for (int j = 0; j < i; j++)
					sum += (LU(k, j) * LU(j, i));

				 
				LU(k, i) = (matrix(k, i) - sum) / LU(i, i);
			}
		}
	}

	return LU;
}

MatrixXd Eigen_LU_Matrix(MatrixXd matrix)
{
	FullPivLU<MatrixXd> lu(matrix);
//	cout << lu.matrixLU() << endl<<endl;
	return lu.matrixLU();

}

double Matrix_Density(int zeros, int size)
{
	double result = double(zeros) / double(size);
	return (1 - result) * 100;
}

void Print_Eq(MatrixXd matrix, VectorXd x, VectorXd b, int precision = 2)
{
	int size = sqrt(matrix.size());
	cout << "Macierz: ";
	std::streamsize ss = std::cout.precision();
	for (int i = 0; i < size-1; i++)
	{
		cout << "\t";
	}
	cout << "Wektor X:";
	cout << "\t Wektor B:\n";

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (matrix(i, j) >= 0) cout << setprecision(precision) << " " << matrix(i, j) << "\t";
			else cout << setprecision(precision) << matrix(i, j) << "\t";

		}
		cout<<setprecision(ss) << " | " << x(i) << "\t = " << b(i) << endl;
	}

}


int main()
{
	
	file.open("results.txt", fstream::out);
	file.clear();
	MatrixXd matrix;
	int num = 64;
	for (int num = 4; num < 256; num *= 2)
	{
		cout << "# Wielkosc macierzy: (" << num << ") " << (num - 2)*(num - 2) << " elementow\n";
		file << "# Wielkosc macierzy: (" << num << ") " << (num - 2)*(num - 2) << " elementow\n";
		matrix = Generate_Matrix(num);
		int zeros = Count_Zeros(matrix);
		cout << "# Liczba zer: " << zeros;
		file << "  Liczba zer: " << zeros;
		double density = Matrix_Density(zeros, matrix.size());
		cout << "  [Gestosc: " << density << "%]\n";
		file << "  [Gestosc: " << density << "%]\n";
		//	cout << "# Wspolczynnik uwarunkowania: " << Con_Factor(matrix) << endl;
		//	MatrixXd LU = Eigen_LU_Matrix(matrix);
		//	cout << "Macierz LU:\n" << LU<< endl;
		VectorXd b = random_B((num - 2)*(num - 2));
		double b_norm = b.norm();
		VectorXd x;
		double relative_error = 0.0;
		cout << "# Uzyta metoda  \tCzas(ms)\tBlad relatywny\n";
		file << "  Uzyta metoda  \tCzas(ms)\tBlad relatywny\n";
		
		if (Matrix_Density(zeros, matrix.size()) < 0.5)	// Konwersja na macierz rzadk� je�li g�sto�� macierzy spadnie ponie�ej 0.5%
		{
			matrix = matrix.sparseView();
			cout << "  > KONWERSJA MACIERZY NA MACIERZ RZAKDA" << endl;
			file << "  > KONWERSJA MACIERZY NA MACIERZ RZAKDA" << endl;
		}

		auto start = chrono::steady_clock::now();
		x = matrix.fullPivLu().solve(b);
		auto end = chrono::steady_clock::now();
		//	Print_Eq(matrix, x, b);
		relative_error = (matrix*x - b).norm() / b_norm;
		cout << "  > LU(FullPivot) \t" << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms \t" << relative_error << endl;
		file << "  > LU(FullPivot) \t" << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms \t" << relative_error << endl;

		start = chrono::steady_clock::now();
		x = matrix.ldlt().solve(b);
		end = chrono::steady_clock::now();
		//	Print_Eq(matrix, x, b);
		relative_error = (matrix*x - b).norm() / b_norm;
		cout << "  > Choleski      \t" << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms \t" << relative_error << endl;
		file << "  > Choleski      \t" << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms \t" << relative_error << endl;

		start = chrono::steady_clock::now();
		x = matrix.fullPivHouseholderQr().solve(b);
		end = chrono::steady_clock::now();
		//	Print_Eq(matrix, x, b);
		relative_error = (matrix*x - b).norm() / b_norm;
		cout << "  > Hausolder(QR)  \t" << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms \t" << relative_error << endl;
		file << "  > Hausolder(QR)  \t" << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms \t" << relative_error << endl;


		start = chrono::steady_clock::now();
		x = matrix.completeOrthogonalDecomposition().solve(b);
		end = chrono::steady_clock::now();
		//	Print_Eq(matrix, x, b);
		relative_error = (matrix*x - b).norm() / b_norm;
		cout << "  > Ortogonal      \t" << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms \t" << relative_error << endl;
		file << "  > Ortogonal      \t" << chrono::duration_cast<chrono::milliseconds>(end - start).count() << " ms \t" << relative_error << endl;
	}
	file.close();
	system("PAUSE");

}