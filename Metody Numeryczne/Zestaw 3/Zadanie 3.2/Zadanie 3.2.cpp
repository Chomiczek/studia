#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <functional>

using namespace std;


vector<double> variables; //  tablica wsp�lczynnik�w przy wielomianie
vector<int> stencil; // tablica siatki dla wzoru: X = (1), O = (0), fX=(2), f=(3)

double polyFunc(double var)
{
	double func = 0.0;
	int power = 0;
	for each (double v in variables)
	{
		func += v*pow(var, power);
		power++;
	}

	return func;
}

// Ustawianie odpowiedniego stencyla (mo�liwo�ci z zadania od 1-5)
void setStencil(int i)
{
	stencil.clear();
	switch (i)
	{
		case 0:	// stencil testowy
			stencil.assign({ 1,2,1 });
			break;
		case 1:
			stencil.assign({ 1,1,2,1,1 });
			break;
		case 2:
			stencil.assign({ 1,1,2,1 });
			break;
		case 3:
			stencil.assign({ 2,1,1,1 });
			break;
		case 4:
			stencil.assign({ 3,1,1,1 });
			break;
		case 5:
			stencil.assign({ 2,1,1 });
			break;
	}
}

void printStencil()
{
	cout << "Stencil[-";
	for each (int var in stencil)
	{
		switch (var)
		{
			case 0:
				cout << "O";
				break;
			case 1:
				cout << "X";
				break;
			case 2:
				cout << "fX";
				break;
			case 3:
				cout << "f";
				break;
		}
		cout << "-";
	}
	cout << "]";
}

// funcja znajduj�ca pierwsz� pochodn� funkcji fx dla warto�ci var 
double derivativeI(function<double(double)> fx, double var, double accuracy = pow(2, -28))
{
	double result = 0.0;

	result = fx(var + accuracy) - fx(var);
	result = result / accuracy;
	return result;
}

double wxPoint(double var, double delta, int searched_position, int current_position)
{
	double result_upper = 1.0;
	double result_bottom = 1.0;
	int i = 0;

	for each (int point in stencil)
	{

		if (i != current_position)
		{
			result_upper *= var - (var + (i - searched_position)*delta);
			result_bottom *= (var + (current_position - searched_position)*delta) - (var + (i - searched_position)*delta);
			//			cout << "\t" << point << " Wykonuje dla: " << i - searched_position << endl;
		}
		i++;
	}

	return result_upper / result_bottom;
}


double wx(function<double(double)> fx, double var, double delta = pow(2, -28))
{
	double result = 0.0;
	int searched_point_position = 0;

	// Znajdowanie pozycji szukanego elementu
	int i = 0;
	for each (int point in stencil)
	{
		if (point == 2 || point == 3)
		{
			searched_point_position = i;
			break;
		}
		i++;
	}

	i = 0;
	for each (int point in stencil)
	{

		if (point != 0 || point != 3)
		{
			//			cout << point<< " Wykonuje dla: " << i - searched_point_position << endl;
			result += fx(var + (i - searched_point_position)*delta)*wxPoint(var, delta, searched_point_position, i);

		}
		i++;
	}
	return result / (2 * delta*delta);
}



double wx0(function<double(double)> fx, double var, double delta = pow(2, -28))	// wz�r dla stencyla nr.0
{
	double result = 0.0;
	int searched_point_position = 0;

	// Znajdowanie pozycji szukanego elementu
	int i = 0;
	for each (int point in stencil)
	{
		if (point == 2 || point == 3)
		{
			searched_point_position = i;
			break;
		}
		i++;
	}

	i = 0;
	for each (int point in stencil)
	{

		if (point != 0 || point != 3)
		{
			result += fx(wxPoint(2 * var, delta, searched_point_position, i));
		}
		i++;
	}
	return result;
}


double wxPrim2(function<double(double)> fx, double var, double delta = pow(2, -28))
{
	double w0 = -3.0 / 2.0;
	double w1 = 2.0;
	double w2 = -1.0 / 2.0;
	return (w0*fx(var - delta) + w1*fx(var) + w2*fx(var + delta)) / delta;
}




int main()
{
	variables.assign({ 1, 2,3});
	
	
	cout << "Function: " << polyFunc(1) << endl;
	cout<< "Normal:\t"<<derivativeI(polyFunc, 1)<<endl;
	double temp;
	for (int i = 0; i < 6; i++)
	{
		setStencil(i);
		temp = derivativeI(polyFunc, wx(polyFunc, 1));
		printStencil(); 
		cout <<":\t" << temp <<  endl;
	}
	

	system("PAUSE");
}