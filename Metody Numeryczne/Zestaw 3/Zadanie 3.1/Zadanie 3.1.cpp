#include <iostream>
#include <cstdio>
#include <cmath>
#include <vector>
#include <functional>

using namespace std;


vector<double> variables; //  tablica wspólczynników przy wielomianie

double polyFunc(double var)
{
	double func = 0.0;
	int power = 0;
	for each (double v in variables)
	{
		func += v*pow(var, power);
		power++;
	}

	return func;
}

// funcja znajdująca pierwszą pochodną funkcji fx dla wartości var 
double derivativeI(function<double(double)> fx, double var, double accuracy=pow(2,-10))
{
	double result = 0.0;

	result = fx(var + accuracy) - fx(var);
	result = result / accuracy;
	return result;
}

// funkcja znajdująca najlepsze przyblizenie pochodnej dla podanej dokładności
double find_best_acc(int var,int start, int end, bool use_accuracy =false, double accuracy=0,bool show_results = true)
{
	vector<double> results;
	double acc = 0;
	int i = start;
	for (; i < end+1; i++)
	{
		acc = pow(2, -i);
		results.push_back(derivativeI(polyFunc,var, acc));
		if(show_results) cout << "2^" << i << "\t| derivative: " << results.back()<<endl;
		if (i>2&& use_accuracy)
			if(abs(results[results.size()-1] - results[results.size() - 2]) < accuracy) break;
	}
	return i;

}


int main()
{
	variables.assign({ 1, 2, 3,4 });
	cout << "Derivative values with accuracy:\nAccuracy| Value\n";
	// Znajdowanie wszytskich warości w zakresie 2^-n, n = 1...64
	find_best_acc(1, 1, 64);



	// Znajdowanie najlepszej dokładności przybliżenia 
	cout << "Best approximation of derivative \n\t with accuracy: 2^-" << find_best_acc(1,1, 64,true, pow(2, -50),false)<<endl<<endl;

	


	system("PAUSE");
}