#include <iostream>
#include <cstdio>
#include <map>
#include <cmath>
#include <fstream>

using namespace std;

#define M_PI 3.14159265358979323846

class Output
{
	fstream file;
public:

	Output(string path = "results.txt")
	{
		file.open(path, fstream::out);
		file.clear();
		file << ("X\tReal\tPolynominal\n");
	}


	// Zapisywanie krotki wynik�w do pliku
	void Write_Line(double var1, double var2, double var3)
	{
		file << var1 << "\t" << var2 << "\t" << var3 << "\t" << "\n";

	}

};


//--------------------------------
// Sinus interpolacj� wielomianow�
//--------------------------------

//Obliczanie iloczynu wyra�e� (x-xi)
double cal_sub_x(map<double, double> sin_var, double var, double x)
{
	double lower = 1.0;
	for (map<double, double>::iterator it = sin_var.begin(); it != sin_var.end(); ++it)
	{
		if (it->first == var) continue;
		lower *= x - it->first;
	}
	return lower;

}

// Obliczanie wyra�enia labda
double cal_labda(map<double, double> sin_var, double var)
{
	return sin_var[var] / cal_sub_x(sin_var, var, var);
}

// Interpolacja wielomianowa
double sin_poly(map<double, double> sin_var, double var)
{
	double poly = 0.0;
	for (map<double, double>::iterator it = sin_var.begin(); it != sin_var.end(); ++it)
	{
		poly += cal_labda(sin_var, it->first)*cal_sub_x(sin_var, it->first, var);
	}
	return poly;
}

// Zwraca ite zero wielomianu Czybryszewa dla podanego stopnia k
double Czybryszew(int k, int zero_index)
{
		return cos(2 * zero_index - 1 * M_PI / 2 * k);

}

//--------------------------------------------------------------------
// Metody sprawdzania b��d�w

// Obliczanie b��du absolutnego
double absolute_e(double func, double aprox)
{
	return abs(func - aprox);
}

// obliczanie b��du wzgl�dnego
double relative_e(double func, double aprox)
{
	return abs(func / aprox - 1);
}

// Sprawdzanie czy nowy b��d absolutny jest maksymalny
void max_absolute_error(double func, double aprox, double &error)
{
	double temp_error = absolute_e(func, aprox);
	if (error < temp_error) error = temp_error;

}

// Sprawdzanie czy nowy b��d relatywny jest maksymalny
void max_relative_error(double func, double aprox, double &error)
{
	if (func != 0.0)
	{
		double temp_error = relative_e(func, aprox);
		if (error < temp_error) error = temp_error;
	}

}
//--------------------------------------------------------------------

void genTrygometric()
{
	Output out = Output();

	map<double, double> sin_var; // lista znanych punkt�w funkcji sin
	int k = 9; // stopie� wielomianu czybryszewa (im wi�kszy tym wi�ksza dok�adno��) 
	
	double temp_var = 0.0;
	for (int i = 1; i <= k; i++) // wype�nianie tablicy znanych warto�ci 
	{
		temp_var = Czybryszew(k,i);
		sin_var[temp_var] = sin(temp_var);
	}

	double var = 0.0;	// Pocz�tkowa warto�� x
	double step = M_PI / 180; // Krok - kolejne dodawane warto�ci do xs


	double polyError_a = 0.0, polyError_r = 0.0;
	double poly, real;
	for (var; var <= M_PI / 2; var += step)
	{
		poly = sin_poly(sin_var, var);
		real = sin(var);


		// Wylicanie maksymalnego b��du
		
		max_absolute_error(real, poly, polyError_a);
		max_relative_error(real, poly, polyError_r);
		
		out.Write_Line(var, real, poly); // Zapis do pliku
	}
	cout << "Sinus value saved in file: results.txt ...\n\n#Max Error Value:\n";
	cout << "Type        \tAbsolute\tRelative\n";
	cout << "Polynominal:\t" << polyError_a << "\t" << polyError_r << endl;
}

int main()
{
	genTrygometric();



	system("PAUSE");
	return 0;
}