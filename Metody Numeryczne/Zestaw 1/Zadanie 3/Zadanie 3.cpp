#include <iostream>
#include <cstdio>
#include <map>
#include <cmath>
#include <fstream>

using namespace std;


// Klasa zapisuj�ca kolejne wyniki do pliku
class Output
{
	fstream file;
public:

	Output(string path = "results.txt")
	{
		file.open(path, fstream::out);
		file.clear();
		file << ("X\tReal\tLinear\tPolynominal\tTylor\n");
	}


	// Zapisywanie krotki wynik�w do pliku
	void Write_Line(double var, double real, double line, double poly, double tylor)
	{
		file << var<<"\t"<<real<<"\t"<<line<<"\t"<<poly<<"\t"<<tylor<<"\n";

	}
	
};

// Pi zdefiniowane do 20 miejsca po przecinku
#define M_PI 3.14159265358979323846

// Sinus interpolacj� liniow�
double sin_linear(map<double, double> sin_var, double var)
{

	pair<double, double> point_1, point_2; // punkty do interpolacji liniowej
	for (map<double, double>::iterator it = sin_var.begin(); it != sin_var.end(); ++it) // znajdowanie najbli�eszgo punktu wi�kszego i mniejszego od podanego
	{
		if (it->first > var)
		{
			point_2 = make_pair(it->first, it->second);
			it--;
			point_1 = make_pair(it->first, it->second);
			break;
		}
	}
	double a_factor = (point_1.second - point_2.second) / (point_1.first - point_2.first); // obliczanie wsp�czynnika a (y=ax+b)
	double b_factor = point_1.second - a_factor*point_1.first;	// obliczanie wsp�czynnika b (y=ax+b)

	return a_factor*var + b_factor; // obliczanie warto�ci dla var


}


//--------------------------------
// Sinus interpolacj� wielomianow�
//--------------------------------

//Obliczanie iloczynu wyra�e� (x-xi)
double cal_sub_x(map<double, double> sin_var, double var, double x)
{
	double lower = 1.0;
	for (map<double, double>::iterator it = sin_var.begin(); it != sin_var.end(); ++it)
	{
		if (it->first == var) continue;
		lower *= x - it->first;
	}
	return lower;

}

// Obliczanie wyra�enia labda
double cal_labda(map<double, double> sin_var, double var)
{
	return sin_var[var] / cal_sub_x(sin_var, var, var);
}

// Interpolacja wielomianowa
double sin_poly(map<double, double> sin_var, double var)
{
	double poly = 0.0;
	for (map<double, double>::iterator it = sin_var.begin(); it != sin_var.end(); ++it)
	{
		poly += cal_labda(sin_var, it->first)*cal_sub_x(sin_var, it->first, var);
	}
	return poly;
}


//--------------------------------
// Sinus rozwini�ciem Tylora
//-------------------------------- 

//Liczenie silni
double fact(double factor)
{
	double var = 1.0;
	for (int i = factor; i > 1; i--)
	{
		var *= i;
	}
	return var;
}

// Interpolacja przy u�yciu rozwini�cia Tylora
double sin_tylor(double var, double accur=10)
{
	double sin = 0.0;
	for (int i = 0; i < accur; i++)
	{
		sin = sin + ((1 / fact(2 * i + 1)) * pow(var, 2 * i + 1) * pow(-1, i));
	}
	return sin;

}


// Obliczanie b��du absolutnego
double absolute_e(double func, double aprox)
{
	return abs(func - aprox);
}

// obliczanie b��du wzgl�dnego
double relative_e(double func, double aprox)
{
	return abs(func / aprox - 1);
}

// Sprawdzanie czy nowy b��d absolutny jest maksymalny
void max_absolute_error(double func, double aprox, double &error)
{
	double temp_error = absolute_e(func, aprox);
	if (error < temp_error) error = temp_error;

}

// Sprawdzanie czy nowy b��d relatywny jest maksymalny
void max_relative_error(double func, double aprox, double &error)
{
	double temp_error = relative_e(func, aprox);
	if (error < temp_error) error = temp_error;

}

int main()
{
	Output out = Output();
	map<double, double> sin_var; // lista znanych punkt�w funkcji sin
	sin_var[0.0] = 0.0;
	sin_var[M_PI / 6] = 0.5;
	sin_var[M_PI / 4] = sqrt(2) / 2;
	sin_var[M_PI / 3] = sqrt(3) / 2;
	sin_var[M_PI / 2] = 1.0;

	double var = 0.0;	// Pocz�tkowa warto�� x
	double step = M_PI/180; // Krok - kolejne dodawane warto�ci do xs
	double real, poly, line, tylor;
	double max_l_a = 0.0, max_l_r = 0.0, max_p_a = 0.0, max_p_r = 0.0, max_t_a = 0.0, max_t_r = 0.0;

	for (var; var <= M_PI / 2; var += step)
		{
			poly = sin_poly(sin_var, var);
			line = sin_linear(sin_var, var);
			tylor = sin_tylor(var);
			real = sin(var);


			// Wylicanie maksymalnego b��du
			max_absolute_error(real, poly, max_p_a);
			max_relative_error(real, poly, max_p_r);
			max_absolute_error(real, line, max_l_a);
			max_relative_error(real, line, max_l_r);
			max_absolute_error(real, tylor, max_t_a);
			max_relative_error(real, tylor, max_t_r);


			out.Write_Line(var, real, line, poly, tylor); // Zapis do pliku
		}
	cout << "Sinus value saved in file: results.txt ...\n\n Max Error Value:\n";
	cout << "Type   \tAbsolute\tRelative\n";
	cout << "Linear:\t" << max_l_a << "\t" << max_l_r << endl;
	cout << "Polyno:\t" << max_p_a << "\t" << max_p_r << endl;
	cout << "Tylor: \t" << max_p_a << "\t" << max_p_r << endl;


	system("PAUSE");
	return 0;
}