#include <iostream>
#include <cstdio>
#include <map>
#include <cmath>
#include <fstream>

using namespace std;

// Pi zdefiniowane do 20 miejsca po przecinku
#define M_PI 3.14159265358979323846

// Klasa zapisuj�ca kolejne wyniki do pliku
class Output
{
	fstream file;
public:

	Output(string path = "results.txt")
	{
		file.open(path, fstream::out);
		file.clear();
		file << ("X\tReal Tan\tApprox Tan\n");
	}


	// Zapisywanie krotki wynik�w do pliku
	void Write_Line(double var1, double var2, double var3)
	{
		file << var1 << "\t" << var2 << "\t" << var3 <<"\n";

	}

};

//--------------------------------
// Sinus rozwini�ciem Tylora
//-------------------------------- 

//Liczenie silni
double fact(double factor)
{
	double var = 1.0;
	for (int i = factor; i > 1; i--)
	{
		var *= i;
	}
	return var;
}

// Interpolacja przy u�yciu rozwini�cia Tylora
double sinTylor(double var, double accur = 100)
{
	double sin = 0.0;
	for (int i = 0; i < accur; i++)
	{
		sin = sin + ((1 / fact(2 * i + 1)) * pow(var, 2 * i + 1) * pow(-1, i));
	}
	return sin;

}

//symetria wzg�dem prostej x=PI/2 => dobicie dla PI/2 do PI
double symmetryY(double var)
{
	return -var + M_PI;
}

// //symetria wzg�dem osi y=> dobicie dla PI do 3PI/2  
double symmetryX(double var)
{
	return var - M_PI;
}

// upraszczanie warto�ci do przedzia�u (0,2PI)
double strip_PI(double var, double to_strip=2*M_PI)
{
	int temp = var / to_strip;
	return var = var - ((to_strip)*temp);
}

// funkcja interpolacji sinusa
double getSin(double var)
{
	var = strip_PI(var);
	if (0 <= var <= M_PI / 2) return sinTylor(var);
	else if (M_PI / 2 < var < M_PI) return sinTylor(symmetryX(var));
	else if (M_PI<var<3 * M_PI / 2) return sinTylor(symmetryY(var))*(-1);
	else if (3 * M_PI / 2 < var < 2 * M_PI) return sinTylor(symmetryY(symmetryX(var)))*(-1);
	else cout << "Podano b��dn� warto�� x dla sin!";
	return 0.0;

}

// funkcja interpolacji cosinusa
double getCos(double var)
{
	return getSin((-1)*(var + (3 * M_PI / 2)));
}

double getTg(double var)
{
	if (strip_PI(var,M_PI/2) == 0 && strip_PI(var, M_PI) != 0) return 0.0;
	else return getSin(var) / getCos(var);
}


//--------------------------------------------------------------------
// Metody sprawdzania b��d�w

// Obliczanie b��du absolutnego
double absolute_e(double func, double aprox)
{
	return abs(func - aprox);
}

// obliczanie b��du wzgl�dnego
double relative_e(double func, double aprox)
{
	return abs(func / aprox - 1);
}

// Sprawdzanie czy nowy b��d absolutny jest maksymalny
void max_absolute_error(double func, double aprox, double &error)
{
	double temp_error = absolute_e(func, aprox);
	if (error < temp_error) error = temp_error;

}

// Sprawdzanie czy nowy b��d relatywny jest maksymalny
void max_relative_error(double func, double aprox, double &error)
{
	double temp_error = relative_e(func, aprox);
	if (error < temp_error) error = temp_error;

}

//--------------------------------------------------------------------

void genTrygometric()
{
	Output out = Output();

	double var = 0.0;	// Pocz�tkowa warto�� x
	double step = M_PI / 180; // Krok - kolejne dodawane warto�ci do xs

	double tgR, tgT;
	double tgError_a = 0.0, tgError_r = 0.0;
	for (var; var <= 2 * M_PI; var += step)
	{
		
		if (strip_PI(var, M_PI / 2) == 0 && strip_PI(var, M_PI) != 0) continue;
		else tgR = tan(var);

		tgT = getTg(var);
		


		// Wylicanie maksymalnego b��du
		max_absolute_error(tgR, tgT, tgError_a);
		max_absolute_error(tgR, tgT, tgError_r);
		

		out.Write_Line(var, tgR, tgT); // Zapis do pliku
	}
	cout << "Tangens value saved in file: results.txt ...\n\n#Max Error Value:\n";
	cout << "Type    \tAbsolute\tRelative\n";
	cout << "Tangens:\t" << tgError_a << "\t" << tgError_r << endl;
	

}

int main()
{
	genTrygometric();



	system("PAUSE");
}