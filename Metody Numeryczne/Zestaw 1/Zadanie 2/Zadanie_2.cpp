#include <iostream>
#include <cstdio>
#include <map>
#include <cmath>
#include <fstream>
#include <chrono>

using namespace std;
// Pi zdefiniowane do 20 miejsca po przecinku
#define M_PI 3.14159265358979323846

// Klasa zapisuj�ca kolejne wyniki do pliku
class Output
{
	fstream file;
public:

	Output(string path = "results.txt")
	{
		file.open(path, fstream::out);
		file.clear();
		file << ("X\tReal\tLinear\tPolynominal\n");
	}


	// Zapisywanie krotki wynik�w do pliku
	void Write_Line(double var1, double var2, double var3, double var4)
	{
		file << var1 << "\t" << var2 << "\t" << var3 << "\t" << var4 << "\n";

	}

};

class float_inter
{
public:
	// Sinus interpolacj� liniow�
	float sin_linear(map<float, float> sin_var, float var)
	{

		pair<float, float> point_1, point_2; // punkty do interpolacji liniowej
		for (map<float, float>::iterator it = sin_var.begin(); it != sin_var.end(); ++it) // znajdowanie najbli�eszgo punktu wi�kszego i mniejszego od podanego
		{
			if (it->first > var)
			{
				point_2 = make_pair(it->first, it->second);
				it--;
				point_1 = make_pair(it->first, it->second);
				break;
			}
		}
		float a_factor = (point_1.second - point_2.second) / (point_1.first - point_2.first); // obliczanie wsp�czynnika a (y=ax+b)
		float b_factor = point_1.second - a_factor*point_1.first;	// obliczanie wsp�czynnika b (y=ax+b)

		return a_factor*var + b_factor; // obliczanie warto�ci dla var


	}


	//--------------------------------
	// Sinus interpolacj� wielomianow�
	//--------------------------------

	//Obliczanie iloczynu wyra�e� (x-xi)
	float cal_sub_x(map<float, float> sin_var, float var, float x)
	{
		float lower = 1.0;
		for (map<float, float>::iterator it = sin_var.begin(); it != sin_var.end(); ++it)
		{
			if (it->first == var) continue;
			lower *= x - it->first;
		}
		return lower;

	}

	// Obliczanie wyra�enia labda
	float cal_labda(map<float, float> sin_var, float var)
	{
		return sin_var[var] / cal_sub_x(sin_var, var, var);
	}

	// Interpolacja wielomianowa
	float sin_poly(map<float, float> sin_var, float var)
	{
		float poly = 0.0;
		for (map<float, float>::iterator it = sin_var.begin(); it != sin_var.end(); ++it)
		{
			poly += cal_labda(sin_var, it->first)*cal_sub_x(sin_var, it->first, var);
		}
		return poly;
	}

	//--------------------------------------------------------------------
	// Metody sprawdzania b��d�w

	// Obliczanie b��du absolutnego
	float absolute_e(float func, float aprox)
	{
		return abs(func - aprox);
	}

	// obliczanie b��du wzgl�dnego
	float relative_e(float func, float aprox)
	{
		return abs(func / aprox - 1);
	}

	// Sprawdzanie czy nowy b��d absolutny jest maksymalny
	void max_absolute_error(float func, float aprox, float &error)
	{
		float temp_error = absolute_e(func, aprox);
		if (error < temp_error) error = temp_error;

	}

	// Sprawdzanie czy nowy b��d relatywny jest maksymalny
	void max_relative_error(float func, float aprox, float &error)
	{
		float temp_error = relative_e(func, aprox);
		if (error < temp_error) error = temp_error;

	}
	//--------------------------------------------------------------------

	void genTrygometric()
	{
		Output out = Output("result_float.txt");

		map<float, float> sin_var; // lista znanych punkt�w funkcji sin
		sin_var[0.0] = 0.0;
		sin_var[M_PI / 6] = 0.5;
		sin_var[M_PI / 4] = sqrt(2) / 2;
		sin_var[M_PI / 3] = sqrt(3) / 2;
		sin_var[M_PI / 2] = 1.0;

		float var = 0.0;	// Pocz�tkowa warto�� x
		float step = M_PI / 180; // Krok - kolejne dodawane warto�ci do xs


		float lineError_a = 0.0, lineError_r = 0.0, polyError_a = 0.0, polyError_r = 0.0;
		for (var; var <= M_PI / 2; var += step)
		{
			float poly = sin_poly(sin_var, var);
			float line = sin_linear(sin_var, var);
			float real = sin(var);


			// Wylicanie maksymalnego b��du
			max_absolute_error(real, line, lineError_a);
			max_relative_error(real, line, lineError_r);
			max_absolute_error(real, poly, polyError_a);
			max_relative_error(real, poly, polyError_r);


			out.Write_Line(var, real, line, poly); // Zapis do pliku
		}
		cout << "#Max Error Value:\n";
		cout << "Type        \tAbsolute\tRelative\n";
		cout << "Linear:     \t" << lineError_a << "\t" << lineError_r << endl;
		cout << "Polynominal:\t" << polyError_a << "\t" << polyError_r << endl;

	}
};

class double_inter
{
public:
	// Sinus interpolacj� liniow�
	double sin_linear(map<double, double> sin_var, double var)
	{

		pair<double, double> point_1, point_2; // punkty do interpolacji liniowej
		for (map<double, double>::iterator it = sin_var.begin(); it != sin_var.end(); ++it) // znajdowanie najbli�eszgo punktu wi�kszego i mniejszego od podanego
		{
			if (it->first > var)
			{
				point_2 = make_pair(it->first, it->second);
				it--;
				point_1 = make_pair(it->first, it->second);
				break;
			}
		}
		double a_factor = (point_1.second - point_2.second) / (point_1.first - point_2.first); // obliczanie wsp�czynnika a (y=ax+b)
		double b_factor = point_1.second - a_factor*point_1.first;	// obliczanie wsp�czynnika b (y=ax+b)

		return a_factor*var + b_factor; // obliczanie warto�ci dla var


	}


	//--------------------------------
	// Sinus interpolacj� wielomianow�
	//--------------------------------

	//Obliczanie iloczynu wyra�e� (x-xi)
	double cal_sub_x(map<double, double> sin_var, double var, double x)
	{
		double lower = 1.0;
		for (map<double, double>::iterator it = sin_var.begin(); it != sin_var.end(); ++it)
		{
			if (it->first == var) continue;
			lower *= x - it->first;
		}
		return lower;

	}

	// Obliczanie wyra�enia labda
	double cal_labda(map<double, double> sin_var, double var)
	{
		return sin_var[var] / cal_sub_x(sin_var, var, var);
	}

	// Interpolacja wielomianowa
	double sin_poly(map<double, double> sin_var, double var)
	{
		double poly = 0.0;
		for (map<double, double>::iterator it = sin_var.begin(); it != sin_var.end(); ++it)
		{
			poly += cal_labda(sin_var, it->first)*cal_sub_x(sin_var, it->first, var);
		}
		return poly;
	}

	//--------------------------------------------------------------------
	// Metody sprawdzania b��d�w

	// Obliczanie b��du absolutnego
	double absolute_e(double func, double aprox)
	{
		return abs(func - aprox);
	}

	// obliczanie b��du wzgl�dnego
	double relative_e(double func, double aprox)
	{
		return abs(func / aprox - 1);
	}

	// Sprawdzanie czy nowy b��d absolutny jest maksymalny
	void max_absolute_error(double func, double aprox, double &error)
	{
		double temp_error = absolute_e(func, aprox);
		if (error < temp_error) error = temp_error;

	}

	// Sprawdzanie czy nowy b��d relatywny jest maksymalny
	void max_relative_error(double func, double aprox, double &error)
	{
		double temp_error = relative_e(func, aprox);
		if (error < temp_error) error = temp_error;

	}
	//--------------------------------------------------------------------

	void genTrygometric()
	{
		Output out = Output("result.double.txt");

		map<double, double> sin_var; // lista znanych punkt�w funkcji sin
		sin_var[0.0] = 0.0;
		sin_var[M_PI / 6] = 0.5;
		sin_var[M_PI / 4] = sqrt(2) / 2;
		sin_var[M_PI / 3] = sqrt(3) / 2;
		sin_var[M_PI / 2] = 1.0;

		double var = 0.0;	// Pocz�tkowa warto�� x
		double step = M_PI / 180; // Krok - kolejne dodawane warto�ci do xs


		double lineError_a = 0.0, lineError_r = 0.0, polyError_a = 0.0, polyError_r = 0.0;
		for (var; var <= M_PI / 2; var += step)
		{
			double poly = sin_poly(sin_var, var);
			double line = sin_linear(sin_var, var);
			double real = sin(var);


			// Wylicanie maksymalnego b��du
			max_absolute_error(real, line, lineError_a);
			max_relative_error(real, line, lineError_r);
			max_absolute_error(real, poly, polyError_a);
			max_relative_error(real, poly, polyError_r);


			out.Write_Line(var, real, line, poly); // Zapis do pliku
		}
		cout << "#Max Error Value:\n";
		cout << "Type        \tAbsolute\tRelative\n";
		cout << "Linear:     \t" << lineError_a << "\t" << lineError_r << endl;
		cout << "Polynominal:\t" << polyError_a << "\t" << polyError_r << endl;

	}
};


int main()
{
	

	float_inter f = float_inter();
	double_inter d = double_inter();


	cout << "#(float)Sin():\n";
	auto f_time_s = chrono::steady_clock::now();
	f.genTrygometric();
	auto f_time = chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now()-f_time_s).count();
	cout<<" \t\t In:"<< f_time << "us" << endl;

	cout << "\n#(double)Sin():\n";
	auto d_time_s = chrono::steady_clock::now();
	d.genTrygometric();
	auto d_time = chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - d_time_s).count();
	cout << " \t\t In:" << d_time << "us" << endl;
	

	cout << "\n\n#Compare(double-float):\n";
	cout << "> Time diffrence(double-float): " << d_time - f_time << "us" << endl;
	

	system("PAUSE");
	return 0;
}