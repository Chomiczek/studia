#include <iostream>
#include <cstdio>
#include <map>
#include <cmath>
#include <fstream>

using namespace std;

// Pi zdefiniowane do 20 miejsca po przecinku
#define M_PI 3.14159265358979323846

// Klasa zapisuj�ca kolejne wyniki do pliku
class Output
{
	fstream file;
public:

	Output(string path = "results.txt")
	{
		file.open(path, fstream::out);
		file.clear();
		file << ("X\tReal Sin\tApprox Sin\tReal Cos\tApprox Cos\n");
	}


	// Zapisywanie krotki wynik�w do pliku
	void Write_Line(double var1, double var2, double var3, double var4, double var5)
	{
		file << var1 << "\t" << var2 << "\t" << var3 << "\t" << var4 << "\t" << var5 << "\n";

	}

};

//--------------------------------
// Sinus rozwini�ciem Tylora
//-------------------------------- 

//Liczenie silni
double fact(double factor)
{
	double var = 1.0;
	for (int i = factor; i > 1; i--)
	{
		var *= i;
	}
	return var;
}

// Interpolacja przy u�yciu rozwini�cia Tylora
double sinTylor(double var, double accur = 100)
{
	double sin = 0.0;
	for (int i = 0; i < accur; i++)
	{
		sin = sin + ((1 / fact(2 * i + 1)) * pow(var, 2 * i + 1) * pow(-1, i));
	}
	return sin;

}

//symetria wzg�dem prostej x=PI/2 => dobicie dla PI/2 do PI
double symmetryY(double var)
{
	return -var + M_PI;
}

// //symetria wzg�dem osi y=> dobicie dla PI do 3PI/2  
double symmetryX(double var)
{
	return var - M_PI;
}

// upraszczanie warto�ci do przedzia�u (0,2PI)
double strip_PI(double var)
{
	int temp = var / (2 * M_PI);
	return var=var-((2*M_PI)*temp);
}

// funkcja interpolacji sinusa
double getSin(double var)
{
	var = strip_PI(var);
	if (0 <= var <= M_PI / 2) return sinTylor(var);
	else if (M_PI / 2 < var < M_PI) return sinTylor(symmetryX(var));
	else if (M_PI<var<3*M_PI/2) return sinTylor(symmetryY(var))*(-1);
	else if (3 * M_PI / 2 < var < 2 * M_PI) return sinTylor(symmetryY(symmetryX(var)))*(-1);
	else cout << "Podano b��dn� warto�� x dla sin!";
	return 0.0;
	
}

// funkcja interpolacji cosinusa
double getCos(double var)
{
	return getSin((-1)*(var + (3*M_PI / 2)));
}

//--------------------------------------------------------------------
// Metody sprawdzania b��d�w

// Obliczanie b��du absolutnego
double absolute_e(double func, double aprox)
{
	return abs(func - aprox);
}

// obliczanie b��du wzgl�dnego
double relative_e(double func, double aprox)
{
	return abs(func / aprox - 1);
}

// Sprawdzanie czy nowy b��d absolutny jest maksymalny
void max_absolute_error(double func, double aprox, double &error)
{
	double temp_error = absolute_e(func, aprox);
	if (error < temp_error) error = temp_error;

}

// Sprawdzanie czy nowy b��d relatywny jest maksymalny
void max_relative_error(double func, double aprox, double &error)
{
	double temp_error = relative_e(func, aprox);
	if (error < temp_error) error = temp_error;

}

//--------------------------------------------------------------------

void genTrygometric()
{
	Output out = Output();

	double var = 0.0;	// Pocz�tkowa warto�� x
	double step = M_PI / 180; // Krok - kolejne dodawane warto�ci do xs

	double sinR, cosR, sinT, cosT;
	double sinError_a=0.0, sinError_r = 0.0, cosError_a = 0.0, cosError_r = 0.0;
	for (var; var <= 2*M_PI; var += step)
	{
		sinR = sin(var);
		cosR = cos(var);

		sinT = getSin(var);
		cosT = getCos(var);


		// Wylicanie maksymalnego b��du
		max_absolute_error(sinR, sinT, sinError_a);
		max_relative_error(sinR, sinT, sinError_r);
		max_absolute_error(cosR, cosT, cosError_a);
		max_relative_error(cosR, cosT, cosError_r);


		out.Write_Line(var, sinR, sinT, cosR, cosT); // Zapis do pliku
	}
	cout << "Sinus and cosinus value saved in file: results.txt ...\n\n#Max Error Value:\n";
	cout << "Type    \tAbsolute\tRelative\n";
	cout << "Sinus:  \t" << sinError_a << "\t" << sinError_r << endl;
	cout << "Cosinus:\t" << cosError_a << "\t" << cosError_r << endl;
	
}

int main()
{
	genTrygometric();



	system("PAUSE");
}