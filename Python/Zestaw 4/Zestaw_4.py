from os import system
import platform

#-------------------------------------------------
# Zadanie 4.2 ------------------------------------
#-------------------------------------------------
# Funkcja z zadania 3.5
def Func_35():
    print "# Zadanie 3.5 - forma funkcji"

    user = None
    print "Prosze podac dlugosc miarki: "
    length = 0
    while True:
        user=raw_input()
        if(user.isdigit()):
            length = int(user)
            break
        else: print "Podano bledna wartosc."
    widith = 0
    print "Prosze podac szerokosc miarki: "
    while True:
        user=raw_input()
        if(user.isdigit()):
            widith = int(user)
            break
        else: Var_Error()
    loop_counter = 0
    yter = 0
    string ="|"
    while loop_counter<length:
    
        while yter<widith: 
            string+="."
            yter+=1
        string+="|"
        yter=0
        loop_counter+=1
    loop_counter=0
    string+="\n"
    while loop_counter<=length:
   
       if(loop_counter!=0):
           while yter<=widith-len(str(loop_counter)): 
               string+= " "
               yter+=1
       yter=0
       string+=str(loop_counter)
       loop_counter+=1
    return string
    

# Tworzenie poziomych linii kwadratu do zadania 3.6
def Get_Upper_Row(widith, string):
    w_iter = 0
    string+="\n+"
    while w_iter<widith: 
        string+="---+"
        w_iter+=1
    return string

# Tworzenie pionowych linii kwadratu do zadania 3.6
def Get_Middle_Row(widith, string):
    w_iter = 0
    string+="\n|"
    while w_iter<widith: 
        string+="   |"
        w_iter+=1
    return string

# Funkcja z zadania 3.6
def Func_36():
    print "# Zadanie 3.6 - forma funkcji"
    user = None
    print "Prosze podac szerokosc(ilosc kwadratow w poziomie): "
    widith = 0
    while True:
        user=raw_input()
        if(user.isdigit()):
            widith = int(user)
            break
        else: print "Podano bledna wartosc."
    heigth = 0
    print "Prosze podac wysokosc(ilosc kwadratow w pionie): "
    while True:
        user=raw_input()
        if(user.isdigit()):
            heigth = int(user)
            break
        else: print "Podano bledna wartosc."

    string = ""
    h_iter = 0
    while h_iter<heigth:
        string=Get_Upper_Row(widith,string)
        string=Get_Middle_Row(widith,string)
        h_iter+=1
    string=Get_Upper_Row(widith,string)
    return string
    
def Zad_2():
    print "# Zadanie 4.2"
    print Func_35()
    print Func_36()
    system("PAUSE")

#-------------------------------------------------
# Zadanie 4.3 ------------------------------------
#-------------------------------------------------
def factorial(number):
    loop_iter=1
    factor = 1
    while loop_iter<=number:
        factor*=loop_iter
        loop_iter+=1
    return factor

def Zad_3():
    print "# Zadanie 4.3\n"
    number = 0
    while True:
        user=raw_input("Prosze podac liczbe: ")
        if(user.isdigit()):
            number = int(user)
            break
        else: print "Podano bledna wartosc."
   
    print "Silnia z " + str(number) + " wynosi: " + str(factorial(number))
    system("PAUSE")

#-------------------------------------------------
# Zadanie 4.4 ------------------------------------
#-------------------------------------------------

def fibbonachi(number):
    fib_0 = 0
    fib_1 = 1
    loop_iter=2
    while loop_iter<=number:
        if(loop_iter%2==0): fib_0 = fib_0+fib_1
        else: fib_1 = fib_0+fib_1
        loop_iter+=1
    if(loop_iter%2==0): return fib_1
    else: return fib_0

def Zad_4(): 
    print "# Zadanie 4.4\n"
   
    number = 0
    while True:
        user=raw_input("Prosze podac liczbe: ")
        if(user.isdigit()):
            number = int(user)
            break
        else: print "Podano bledna wartosc."
    print str(number) + " liczba fibonacciego: " +str(fibbonachi(number))

    system("PAUSE")

#-------------------------------------------------
# Zadanie 4.5 ------------------------------------
#-------------------------------------------------
def odwracanie_iter(L,left,right):
    while left<right:
        loop_iter=left
        while loop_iter<right:
            temp = L[loop_iter]
            L[loop_iter] = L[loop_iter+1]
            L[loop_iter+1] = temp
            loop_iter+=1
        right-=1

def odwracanie_reku_step(L,left,right):
    new_L=[]
    temp =[L[left]]
    if(left!=right): new_L = odwracanie_reku_step(L,left+1,right)
    new_L= new_L+temp
    return new_L
   
def odwracanie_reku(L,left,right):  
    return L[0:left]+odwracanie_reku_step(L,left,right)+L[right+1:len(L)]     

def Zad_5(): 
    print "# Zadanie 4.5\n"
    L = ["A","B","C","D","E"]
    odwracanie_iter(L,1,3)
    print L
    L = ["A","B","C","D","E"]
    print odwracanie_reku(L,1,3)

    system("PAUSE")


#-------------------------------------------------
# Zadanie 4.6 ------------------------------------
#-------------------------------------------------
def sum_seq(sequence):
    value = 0

    for element in sequence:
        if(isinstance(element,(list,tuple))): value+=sum_seq(element)
        else: value+= int(element)

    return value

def Zad_6(): 
    print "# Zadanie 4.6\n"
    L = ["2","22",["6", "666"],"4"]
    print L
    print " Suma sekwencji wynosi: "+ str(sum_seq(L))

    system("PAUSE")

#-------------------------------------------------
# Zadanie 4.7 ------------------------------------
#-------------------------------------------------
def flatten(sequence):
    new_L = list()
    for element in sequence:
        if(isinstance(element,(list,tuple))): 
            new_L = new_L + flatten(element)
        else:
            new_L.append(element)
    return new_L

def Zad_7(): 
    print "# Zadanie 4.7\n"
    seq = [1,(2,3),[],[4,(5,6,7)],8,[9]]
    print seq
    print "Wygladzono: "
    print flatten(seq)

    system("PAUSE")

#-------------------------------------------------
# Funkcje uniwesalne oraz menu -------------------
#-------------------------------------------------


# Funkcja czyszczenia konsoli
def Clear():
    if(platform.system() == "Windows"): system("cls")
    else: system("clear")

# Funkcja menu wyboru
def Menu():
    
    user = None
    while user!="end":
        Clear()
        print "##### Menu #####"
        print "Prosze wpisac numer zadania i nacisnac enter. \nBy zakonczyc program prosze wpisac [end]."
        print "> 2 - Zadanie 4.2"  
        print "> 3 - Zadanie 4.3" 
        print "> 4 - Zadanie 4.4"
        print "> 5 - Zadanie 4.5"
        print "> 6 - Zadanie 4.6"
        print "> 7 - Zadanie 4.7"    
        user = raw_input()
        Clear()
        if(user.isdigit()):
            if(int(user) == 2): Zad_2()
            elif(int(user) == 3): Zad_3()
            elif(int(user) == 4): Zad_4()
            elif(int(user) == 5): Zad_5()
            elif(int(user) == 6): Zad_6()
            elif(int(user) == 7): Zad_7()

Menu()