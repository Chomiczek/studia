from os import system
import platform
#-------------------------------------------------
# Zadanie 3.3 ------------------------------------
#-------------------------------------------------
def Zad_3():
    print "# Zadanie 3.3"
    iter = 0
    string = ""
    while iter<31:
        if(iter%3!=0): print iter
        iter+=1
    system("PAUSE")

#-------------------------------------------------
# Zadanie 3.4 ------------------------------------
#-------------------------------------------------
# Funkcja obliczajaca trzecia potege
def x3(num):
        num3 = int(num)*int(num)*int(num)
        print " Pobrano: " + str(num) + ". Trzecia potega: " + str(num3)

def Zad_4():
    print "# Zadanie 3.4"

    
    user = None

    while user!="stop":
        print "\nProsze podac numer lub wpisac [stop] by zatrzymac:"
        user=raw_input()
        if(user.isdigit()): x3(user)
        elif(user!="stop"): Var_Error()
   

#-------------------------------------------------
# Zadanie 3.5 ------------------------------------
#-------------------------------------------------
def Zad_5():
    print "# Zadanie 3.5"

    user = None
    print "Prosze podac dlugosc miarki: "
    length = 0
    while True:
        user=raw_input()
        if(user.isdigit()):
            length = int(user)
            break
        else: Var_Error()
    widith = 0
    print "Prosze podac szerokosc miarki: "
    while True:
        user=raw_input()
        if(user.isdigit()):
            widith = int(user)
            break
        else: Var_Error()
    iter = 0
    yter = 0
    string ="|"
    while iter<length:
    
        while yter<widith: 
            string+="."
            yter+=1
        string+="|"
        yter=0
        iter+=1
    iter=0
    string+="\n"
    while iter<=length:
   
       if(iter!=0):
           while yter<=widith-len(str(iter)): 
               string+= " "
               yter+=1
       yter=0
       string+=str(iter)
       iter+=1
    print string
    system("PAUSE")

#-------------------------------------------------
# Zadanie 3.6 ------------------------------------
#-------------------------------------------------
# Tworzenie poziomych linii kwadratu
def Get_Upper_Row(widith, string):
    w_iter = 0
    string+="\n+"
    while w_iter<widith: 
        string+="---+"
        w_iter+=1
    return string

# Tworzenie pionowych linii kwadratu
def Get_Middle_Row(widith, string):
    w_iter = 0
    string+="\n|"
    while w_iter<widith: 
        string+="   |"
        w_iter+=1
    return string

def Zad_6():
    print "# Zadanie 3.6"
    user = None
    print "Prosze podac szerokosc(ilosc kwadratow w poziomie): "
    widith = 0
    while True:
        user=raw_input()
        if(user.isdigit()):
            widith = int(user)
            break
        else: Var_Error()
    heigth = 0
    print "Prosze podac wysokosc(ilosc kwadratow w pionie): "
    while True:
        user=raw_input()
        if(user.isdigit()):
            heigth = int(user)
            break
        else: Var_Error()

    string = ""
    h_iter = 0
    while h_iter<heigth:
        string=Get_Upper_Row(widith,string)
        string=Get_Middle_Row(widith,string)
        h_iter+=1
    string=Get_Upper_Row(widith,string)
    print string
    system("PAUSE")

#-------------------------------------------------
# Zadanie 3.8 ------------------------------------
#-------------------------------------------------
def Zad_8():
    print "# Zadanie 3.8"
    L1= ["aaa","abc","a","d","f","g","h","666","rozkaz 66","h"]
    L2= ["aab","abc","ag","c","d","f","666","rozkaz 66", "h","Hello There"]

    print L1
    print L2

    print "Lista elementow wspolnych (bez powtorzen):"
    element_list = set()
    for element_1 in L1:
        for element_2 in L2:
            if(element_1 == element_2): element_list.add(element_1)
    print element_list
    print "Lista elementow Z obu sekwencji (bez powtorzen):"
    element_list = set()
    for element_1 in L1: element_list.add(element_1)
    for element_2 in L2: element_list.add(element_2)
    print element_list
    
    system("PAUSE")         

#-------------------------------------------------
# Zadanie 3.9 ------------------------------------
#-------------------------------------------------

# Sprawdzanie czy element jest w zbiorze
def Contains(el_set, element):
    contain = False
    for el in el_set:
        if(el==element):
            contain = True
            break
    return contain

# Sprawdzanie czy podany string jest zlozony z liczb rzymskich
def Check_Roman(in_string):
    
    roman_set = frozenset(['I','V','X','L','C','D','M'])
    contain = True
    for symbol in in_string:
        if(Contains(roman_set,symbol.upper())==False): contain = False
    return contain 
# Funkcja tlumaczaca z rzymskich na arabskie
def Translate(roman):
    values={'M': 1000, 'D': 500, 'C': 100, 'L': 50, 'X': 10, 'V': 5, 'I': 1}
    numbers= []
    for char in roman: numbers.append(values[char.upper()])

    total = 0
    iter=0
    while iter<len(numbers)-1:
        if(numbers[iter]>= numbers[iter+1]): total+= numbers[iter] 
        else: total-= numbers[iter] 
        iter+=1
    total+= numbers[iter]
    return total 

def Zad_9():
    print "# Zadanie 3.9"
    user = None
    number = 0
    while user!="end":
        print "Prosze podac numer w systemie rzymskim.\nBy wyjsc prosze wpisac [end]"
        user=raw_input("Liczba: ")
        if(Check_Roman(user)): print "Liczba " + str(user) + " w systemie arabskim: " +str(Translate(str(user)))
        elif(user!="end"): Var_Error()
   
#-------------------------------------------------
# Funkcje uniwesalne oraz menu -------------------
#-------------------------------------------------
# Funkcja obslugi bledu wejscia
def Var_Error(): print "Podano bledna wartosc."

# Funkcja czyszczenia konsoli
def Clear():
    if(platform.system() == "Windows"): system("cls")
    else: system("clear")

# Funkcja menu wyboru
def Menu():
    
    user = None
    while user!="end":
        Clear()
        print "##### Menu #####"
        print "Prosze wpisac numer zadania i nacisnac enter. \nBy zakonczyc program prosze wpisac [end]."
        print "> 3 - Zadanie 3.3"  
        print "> 4 - Zadanie 3.4"
        print "> 5 - Zadanie 3.5"
        print "> 6 - Zadanie 3.6"
        print "> 8 - Zadanie 3.8"
        print "> 9 - Zadanie 3.9"
        user = raw_input()
        Clear()
        if(user.isdigit()):
            if(int(user) == 3): Zad_3()
            elif(int(user) == 4): Zad_4()
            elif(int(user) == 5): Zad_5()
            elif(int(user) == 6): Zad_6()
            elif(int(user) == 8): Zad_8()
            elif(int(user) == 9): Zad_9()

Menu()
