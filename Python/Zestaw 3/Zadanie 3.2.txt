L = [3, 5, 4] ; L = L.sort()	# L nie musi by� powownie przypisywane po sortowaniu (samo L.sort() wystarcza)
x, y = 1, 2, 3			# przypisywanie nast�puje do dw�ch zmiennych (a s� trzy wartosci).
X = 1, 2, 3 ; X[1] = 4		# Brak [] przy deklaracji listy
X = [1, 2, 3] ; X[3] = 4	# Indeks poza zakresem
X = "abc" ; X.append("d")	# X nie jest list� a stringiem (wi�c append zwr�ci b��d)
map(pow, range(8))		# 