class Frac:

    def __init__(self,count,denom):
        simplify = self.nwd(count,denom)
        self.count = count/simplify     # licznik
        self.denom = denom/simplify     # mianownik

    def __str___(self):
        if(self.denom==1): return str(self.count)
        else: return  str(self.count)+"/"+str(self.denom)
    
    def __repr___(self):
        return  "Frac("+str(self.count)+", "+str(self.denom)+")"

   
    def __eq__(self,other):
        if(self.count == other.count) and (self.denom == other.denom): return True
        else: return False
    
    def __ne__(self,other):
        if(self == other): return False
        else: return True
    
    def nwd(self,num1,num2):
        rem=None
        while num2!=0:
            rem = num1%num2
            num1=num2
            num2= rem
        return num1

    def __add__(self, other):              # frac1 + frac2
        if (self.denom != other.denom):
            return Frac(self.count * other.denom + other.count * self.denom,self.denom * other.denom)
        else:
            return Frac(self.count + other.count,self.denom)

    def __sub__(self, other):             # frac1 - frac2
        if (self.denom != other.denom):
            return Frac(self.count * other.denom - other.count * self.denom,self.denom * other.denom)
        else:
            return Frac(self.count - other.count,self.denom)

    def __mul__(self, other):             # frac1 * frac2
        return Frac(self.count * other.count,self.denom * other.denom)

    def __div__(self, other):             # frac1 / frac2
        return Frac(self.count * other.denom,self.denom * other.count)

    def is_positive(self):                  # bool, czy dodatni
        if(self.count>=0) and (self.denom>0): return True
        else: return False

    def is_zero(self):                      # bool, typu [0, x]
        if(self.count==0) and (self.denom!=0): return True
        else: return False

    def __cmp__(self, other):             # -1 | 0 | +1
       if(self.__float__() > other.__float__()): return -1
       elif(self.__float__() == other.__float__()): return 0
       else: return 1
   
    def __float__(self):                   # konwersja do float
            return float(self.count)/float(self.denom)
   
    def __pos__(self):  # +frac = (+1)*frac
        return self

    def __neg__(self):  # -frac = (-1)*frac
        return Frac(-self.count, self.denom)

    def __invert__(self):  # odwrotnosc: ~frac
        return Frac(self.denom, self.count)


# Kod testuj�cy modu�.

import unittest

class TestFrac(unittest.TestCase): 
   
    def setUp(self):
        pass

    def test_add(self):
        self.assertEqual(Frac(1,2)+(Frac(1, 3)), Frac(5, 6))

    def test_sub(self): 
        self.assertEqual(Frac(1, 2)-(Frac(1, 4)), Frac(1, 4))

    def test_mul(self): 
        self.assertEqual(Frac(1, 2)*(Frac(1, 3)), Frac(1, 6))

    def test_div_frac(self): 
        self.assertEqual(Frac(1, 2)/(Frac(1, 3)), Frac(3, 2))

    def test_is_positive(self): 
        self.assertTrue(Frac(1,2).is_positive())
        self.assertFalse(Frac(-1,2).is_positive())
        self.assertFalse(Frac(1,-2).is_positive())
        self.assertTrue(Frac(-1,-2).is_positive())

    def test_is_zero(self):
        self.assertTrue(Frac(0,2).is_zero())
        self.assertFalse(Frac(4,3).is_zero())

    def test_cmp(self): 
         self.assertEqual(Frac(1, 2).__cmp__(Frac(1, 3)), -1)
         self.assertEqual(Frac(1, 3).__cmp__(Frac(1, 2)), 1)
         self.assertEqual(Frac(1, 2).__cmp__(Frac(1, 2)), 0)
         self.assertEqual(Frac(1, -3).__cmp__(Frac(1, -2)), -1)

    def test_float(self): 
        self.assertEqual(Frac(1, 2).__float__(), 0.5)
        self.assertEqual(Frac(1, 4).__float__(), 0.25)

    def test_invert(self): 
        self.assertEqual(Frac(1, 2).__invert__(), Frac(2,1))
   
    def test_neg(self): 
        self.assertEqual(Frac(1, 2).__neg__(), Frac(-1,2))
    def tearDown(self): pass

# funkcja pozwalajaca rozpoczac testy z poziomu programu
def Start_Test():
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFrac)
    unittest.TextTestRunner(verbosity=2).run(suite)



#Start_Test()
if __name__ == '__main__': unittest.main()









    









