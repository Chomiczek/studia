import math

class Point:

    def __init__(self, x, y):  # konstuktor
        self.x = x
        self.y = y

    def __str__(self):              # zwraca string "(x, y)"
        return "("+str(self.x) + "," + str(self.y)+")"

    def __repr__(self):             # zwraca string "Point(x, y)"
         return "Point("+str(self.x) + "," + str(self.y)+")"

    def __eq__(self, other):       # obs�uga point1 == point2
        if(self.x == other.x) and (self.y == other.y): return True
        else: return False

    def __ne__(self, other):       # obs�uga point1 != point2
        return not self == other

    # Punkty jako wektory 2D.
    def __add__(self, other):     # v1 + v2
        self.x+=other.x
        self.y+=other.y
        return self

    def __sub__(self, other):     # v1 - v2
        self.x-=other.x
        self.y-=other.y
        return self

    def __mul__(self, other):     # v1 * v2, iloczyn skalarny
        return self.x*self.x+other.y*other.y

    def cross(self, other):         # v1 x v2, iloczyn wektorowy 2D
        return self.x * other.y - self.y * other.x

    def length(self):           # d�ugo�� wektora
        return math.sqrt(self.x*self.x + self.y*self.y)

# Kod testuj�cy modu�.

import unittest

class TestPoint(unittest.TestCase): 
   
    def setUp(self): pass
        

    def test_add(self): 
        self.assertEqual(Point(3,2).__add__(Point(4,1)), Point(7,3))

    def test_sub(self): 
        self.assertEqual(Point(3,2).__sub__(Point(4,1)), Point(-1,1))

    def test_mul(self): 
        self.assertEqual(Point(3,2).__mul__(Point(4,1)), 10)
        
    def test_cross(self): 
        self.assertEqual(Point(3,2).cross(Point(4,1)), -5)
    
    def test_length(self): 
        self.assertEqual(Point(3,2).length(), math.sqrt(13))

# funkcja pozwalajaca rozpoczac testy z poziomu programu
def Start_Test():
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPoint)
    unittest.TextTestRunner(verbosity=2).run(suite)

# wywo�anie test�w
if __name__ == '__main__':
    unittest.main()

