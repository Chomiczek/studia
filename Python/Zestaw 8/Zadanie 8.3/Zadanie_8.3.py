import random

# klasa definiujaca punkty
class Point: 
   
    def __init__(self,x_value ,y_value):
        self.x = x_value
        self.y = y_value

    def get_x(self): return self.x
    def get_y(self): return self.y

# zwraca losowy punkt w przedziale
def random_point(first,last):
    x = random.uniform(first,last)
    y = random.uniform(first,last)
    return Point(x,y)

# sprawdza czy podany punkt miesci si� w kole podaym za pomoca srodka (center) oraz promienia (ray)
def in_circle(ray,point,center=Point(0,0)):
    ray2 = ray*ray
    len_x = point.get_x()-center.get_x()
    len_y = point.get_y()-center.get_y()
    len_x*=len_x
    len_y*=len_y
    if(ray2 >= len_x+len_y): return True
    else: return False

# obliczanie pi metoda Monte Carlo
def calc_pi(n=100):
    i=0
    center = Point(0,0)
    ray = 4
    points_in_circle = 0
    while(i!=n):
        i=i+1
        temp = random_point(center.get_x(),ray)
        if(in_circle(ray,temp,center)): points_in_circle+=1
#        if(i%1000==0): print "Wykonano "+ str(i)

    pi = 4.0*points_in_circle/n
    print "Dla " + str(n) + "\t punkt�w PI= "+ str(pi)


print "Obliczanie PI metoda Monte Carlo"
n=10.0
while(n<1000000):
    calc_pi(n)
    n=n*10

print "Zakonczono . . ."