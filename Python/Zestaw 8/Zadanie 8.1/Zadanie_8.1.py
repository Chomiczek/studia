import math





def solve2(a,b,c):
    if(a!=0):
        delta = b*b-4*a*c
        if(delta<0): print "Brak rozwiazan rzeczywistych rownania "+str(a)+"x^2+"+str(b)+"x+"+str(c)+"=0"
        else:
            x1 = (-b-math.sqrt(delta))/(2*a)
            x2 = (-b+math.sqrt(delta))/(2*a)
            print "Rozwiazaniami rownania "+str(a)+"x^2+"+str(b)+"x+"+str(c)+"=0 sa: \n\t x1 = "+str(x1)+"\n\t x2 = "+str(x2)+"\n"
    else:
        x1=-c/b
        print "Rozwiazaniami rownania "+str(a)+"x^2+"+str(b)+"x+"+str(c)+"=0 jest: \n\t x1 = "+str(x1)+"\n"
