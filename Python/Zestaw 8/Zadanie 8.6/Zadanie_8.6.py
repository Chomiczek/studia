import time

#P(0, 0) = 0.5,
#P(i, 0) = 0.0 dla i > 0,
#P(0, j) = 1.0 dla j > 0,
#P(i, j) = 0.5 * (P(i-1, j) + P(i, j-1)) dla i > 0, j > 0.


# rekurencyjna wersja p
def req_p(i,j):
    if(i==0) and(j==0): return 0.5
    elif(j==0): return 0.0
    elif(i==0): return 1.0
    else: return 0.5*(req_p(i-1,j)+req_p(i,j-1))

# dynamiczna wersja p
def dyn_p(i,j):
    p = {}
    p[(0,0)] = 0.5
    
    if(i>=j): r = i
    else: r=j 
    
    for c in range(r+1): 
        p[(c,0)] = 0.0 
        p[(0,c)] = 1.0

    c1 = 1
    c2 = 1
    try:
        while(c1<=i):
            while(c2<=j):
                p[(c1,c2)] = 0.5*(p[(c1-1,c2)]+p[(c1,c2-1)])
                c2+=1
            c1+=1
            c2=1
    except: print "Problem dla: (" + str(c1)+","+str(c2)+") -> " + str(p[(c1-1,c2)]) + " + " + str(p[(c1,c2-1)])
    return p[(i,j)]


i = 10
j = 10
print "Porownanie wartosci: "
t0 = time.clock()
print "\t Rekurencja: \t "+str(req_p(i,j)) + "\t w czasie: " + str(time.clock()-t0) 
t0 = time.clock()
print "\t Dynamicze:  \t "+str(dyn_p(i,j)) + "\t w czasie: " + str(time.clock()-t0) 
    