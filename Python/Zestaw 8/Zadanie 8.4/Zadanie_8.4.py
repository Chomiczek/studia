import math

def triangle_condition(a,b,c):
    if(a+b<c) or (a+c<b) or (b+c<a): raise ValueError("Podano bledne wartosci bokow trojkata") 

def heron(a, b, c):
   triangle_condition(a,b,c)
   perimiter = 0.5*(a+b+c)
   area = math.sqrt(perimiter*(perimiter-a)*(perimiter-b)*(perimiter-c))
   return area

print "Program obliczajacy pole trojkata wzorem Herona"
print "Pole trojkata egipskiego wynosi: " + str(heron(3,4,5))