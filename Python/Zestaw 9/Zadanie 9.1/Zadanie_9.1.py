class Node:
    """Klasa reprezentuj�ca w�ze� listy jednokierunkowej."""

    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)   # bardzo og�lnie

def remove_head(node):
    if(node ==None): raise ValueError("Podano pusta liste")
    else:
        new_head = node.next
        node_removed = node
        return new_head, node_removed

def remove_tail(node): 
    if(node ==None): raise ValueError("Podano pusta liste")
    else:
        head = node
        while head.next.next:
            head = head.next
        node_removed = head.next
        head.next = None
        return node, node_removed

""" Odkomentowac dla spradzenia dzialania 


# R�czne budowanie d�u�szej listy.
head = None                   # [], pusta lista
head = Node(3, head)          # [3]
head = Node(2, head)          # [2, 3]
head = Node(4, head)          # [4, 2, 3]

try:
    # Zastosowanie
    head, node = remove_head(head)
    print "usuwam", node
    head, node = remove_tail(head)
    print "usuwam", node
except Exception as e: print "Blad: "+ str(e)   

"""