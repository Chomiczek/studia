from Queue import Queue

class Node:
    """Klasa reprezentuj�ca w�ze� drzewa binarnego."""

    def __init__(self, data=None, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    def __str__(self):
        return str(self.data)


def bst_max(top):
    if(top == None): raise ValueError("Podano puste drzewo")
    else:
        Q = Queue()
        Q.put(top)
        max_val = top.data
        max_node = top
        while not Q.empty():
            node = Q.get()
            if(max_val<node.data):
                max_val = node.data
                max_node = node
            if node.left:
                Q.put(node.left)
            if node.right:
                Q.put(node.right)
        print "Znaleziono najwieksza wartosc: " + str(max_val)
        return max_node

def bst_min(top):
    if(top == None): raise ValueError("Podano puste drzewo")
    else:
        Q = Queue()
        Q.put(top)
        min_val = top.data
        max_node = top
        while not Q.empty():
            node = Q.get()
            if(min_val>node.data):
                min_val = node.data
                max_node = node
            if node.left:
                Q.put(node.left)
            if node.right:
                Q.put(node.right)
        print "Znaleziono najmniejsza wartosc: " + str(min_val)
        return max_node



""" Odkomentowac by sprawdzic dzialanie


root = None           # puste drzewo
root = Node("start")  # drzewo z jednym w�z�em
# R�czne budowanie wi�kszego drzewa.
root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)
root.right.left = Node(6)
root.right.right = Node(7)

bst_max(root)
bst_min(root)


"""