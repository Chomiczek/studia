import random



def generate_list(number,max_num):
    list = []
    for i in range(number):
        list.append(random.randint(0,max_num));
    return list

def binary_search(L, left, right, y):
    """Wyszukiwanie binarne w wersji iteracyjnej."""
    while left <= right:
        k = (left+right)/2
        if y == L[k]:
            return k
        if y > L[k]:
            left = k+1
        else:
            right = k-1
    return None


def binarne_rek(L, left, right, y):
    """Wyszukiwanie binarne w wersji rekurencyjnej."""
    k = (left+right)/2
    if y == L[k]:
            return k
    if(left >= right): return None
    if(y>L[k]): return binarne_rek(L,k+1,right,y)
    else: return binarne_rek(L,left,k-1,y)



list = generate_list(50,10);
#print("Lista: " + str(list));

s = random.randint(0,10)  
list.sort();
print("Sortuje liste: " + str(list));
print("Wyszukiwanie "+str(s) + " . . .  \nZnaleziono na pozycji:")
print("\tIteracyjnie:   " + str(binary_search(list,0,len(list)-1,s)))
print("\tRekurencyjnie: " + str(binarne_rek(list,0,len(list)-1,s)))
