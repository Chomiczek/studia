import random



def generate_list(number,max_num):
    list = []
    for i in range(number):
        list.append(random.randint(0,max_num));
    return list


def search_for(searched, list):
    found = []
    for i in range(len(list)):
        if(searched==list[i]):
            found.append(i)
    return found


n = 100
k=10
list = generate_list(n,k)
#print("Lista:"+str(list))
searched = random.randint(0,k)
print("Szukam: "+str(searched))
found = search_for(searched,list)
print("\tZnaleziono na pozycjach:"+str(found))

