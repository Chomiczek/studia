from os import system
import platform
import fracs
import Test

# Tworzenie nowego ulamka
def New_Frac():
    count = Get_Input("Prosze podac licznik: ")
    denom = Get_Input("Prosze podac mianownik: ")
    return [count,denom]

#-------------------------------------------------
# Funkcje uniwesalne oraz menu -------------------
#-------------------------------------------------
# Funkcja pobierajaca z klawiatury
def Get_Input(string="Prosze podac wartosc: "):
    number = 0
    while True:
        user=raw_input(string)
        if(user.isdigit()):
            number = int(user)
            break
        else: print "Podano bledna wartosc"
    return number

# Funkcja czyszczenia konsoli
def Clear():
    if(platform.system() == "Windows"): system("cls")
    else: system("clear")

# Funkcja menu wyboru
def Menu():
    print "# Zadanie 5.2"
    user = None
    while user!="end":
        Clear()
        print "##### Menu #####"
        print "Prosze wpisac numer zadania i nacisnac enter. \nBy zakonczyc program prosze wpisac [end]."
        print "> 1 - Uruchom testy"  
        print "> 2 - Dodaj ulamki"
        print "> 3 - Odejmij ulamki" 
        print "> 4 - Mnozenie ulamkow" 
        print "> 5 - Dzielenie ulamkow" 
        user = raw_input()
        Clear()
        if(user.isdigit()):
            if(int(user) == 1): 
                Test.Start_Test()
                system("PAUSE")
            elif(int(user) == 2):
                print "# Dodawanie ulamkow\n" 
                f1 = New_Frac()
                f2 = New_Frac()
                print " Wynik: " + fracs.to_string(fracs.add_frac(f1,f2))
                system("PAUSE")
            elif(int(user) == 3):
                print "# Odejmowanie ulamkow\n" 
                f1 = New_Frac()
                f2 = New_Frac()
                print " Wynik: " + fracs.to_string(fracs.sub_frac(f1,f2))
                system("PAUSE")
            elif(int(user) == 4): 
                print "# Mnozenie ulamkow\n" 
                f1 = New_Frac()
                f2 = New_Frac()
                print " Wynik: " + fracs.to_string(fracs.mul_frac(f1,f2))
                system("PAUSE")
            elif(int(user) == 5): 
                print "# Dzielenie ulamkow\n" 
                f1 = New_Frac()
                f2 = New_Frac()
                print " Wynik: " + fracs.to_string(fracs.div_frac(f1,f2))
                system("PAUSE")
            

Menu()


