
def nwd(num1,num2):
    rem=None
    while num2!=0:
        rem = num1%num2
        num1=num2
        num2= rem
    return num1

def sipmlyfy(frac):
    simple = nwd(frac[0],frac[1])
    frac[0] = frac[0]/simple
    frac[1] = frac[1]/simple
    return frac

def to_string(frac):
        return "["+ str(frac[0])+"/"+str(frac[1])+"]"


def add_frac(frac1, frac2):              # frac1 + frac2
    frac1=sipmlyfy(frac1)
    frac2=sipmlyfy(frac2)
    if (frac1[1] != frac2[1]):
        frac1[0] = frac1[0] * frac2[1]
        frac2[0] = frac2[0] * frac1[1]
        return sipmlyfy([frac1[0] + frac2[0],frac1[1] * frac2[1]])
    else:
        return sipmlyfy([frac1[0] + frac2[0],frac1[1]])

def sub_frac(frac1, frac2):             # frac1 - frac2
    frac1=sipmlyfy(frac1)
    frac2=sipmlyfy(frac2)
    if (frac1[1] != frac2[1]):
        frac1[0] = frac1[0] * frac2[1]
        frac2[0] = frac2[0] * frac1[1]
        return sipmlyfy([frac1[0] - frac2[0],frac1[1] * frac2[1]])
    else:
        return sipmlyfy([frac1[0] - frac2[0],frac1[1]])

def mul_frac(frac1, frac2):             # frac1 * frac2
    frac1=sipmlyfy(frac1)
    frac2=sipmlyfy(frac2)
    return sipmlyfy([frac1[0] * frac2[0],frac1[1] * frac2[1]])
    
def div_frac(frac1, frac2):             # frac1 / frac2
    frac1=sipmlyfy(frac1)
    frac2=sipmlyfy(frac2)
    return sipmlyfy([frac1[0] * frac2[1],frac1[1] * frac2[0]])

def is_positive(frac):                  # bool, czy dodatni
    frac=sipmlyfy(frac);
    if(frac[0]>=0) and (frac[1]>=0): return True
    else: return False

def is_zero(frac):                      # bool, typu [0, x]
    frac=sipmlyfy(frac)
    if(frac[0]==0) and (frac[1]!=0): return True
    else: return False

def cmp_frac(frac1, frac2):             # -1 | 0 | +1
    frac1=sipmlyfy(frac1)
    frac2=sipmlyfy(frac2)
    if(frac1[1] != frac2[1]):
        frac1[0] = frac1[0] * frac2[1]
        frac2[0] = frac2[0] * frac1[1]
    if(frac1[0] > frac2[0]): return -1
    elif(frac1[0] == frac2[0]): return 0
    else: return 1

def frac2float(frac):                   # konwersja do float
    return float(frac[0])/float(frac[1])

