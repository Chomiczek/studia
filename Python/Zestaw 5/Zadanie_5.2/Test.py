import unittest
import fracs

class TestFractions(unittest.TestCase):

    def setUp(self):
        pass

    def test_add_frac(self):
        self.assertEqual(fracs.add_frac([1, 2],[1, 3]), [5, 6])

    def test_sub_frac(self): 
        self.assertEqual(fracs.sub_frac([1, 2], [1, 4]), [1, 4])

    def test_mul_frac(self): 
        self.assertEqual(fracs.mul_frac([1, 2], [1, 3]), [1, 6])

    def test_div_frac(self): 
        self.assertEqual(fracs.div_frac([1, 2], [1, 3]), [3, 2])

    def test_is_positive(self): 
        self.assertTrue(fracs.is_positive([1, 2]))
        self.assertFalse(fracs.is_positive([-1, 2]))
        self.assertFalse(fracs.is_positive([1, -2]))

    def test_is_zero(self):
        self.assertTrue(fracs.is_zero([0, 2]))
        self.assertFalse(fracs.is_zero([2, 4]))

    def test_cmp_frac(self): 
         self.assertEqual(fracs.cmp_frac([1, 2], [1, 3]), -1)
         self.assertEqual(fracs.cmp_frac([1, 3], [1, 2]), 1)
         self.assertEqual(fracs.cmp_frac([1, 2], [1, 2]), 0)

    def test_frac2float(self): 
        self.assertEqual(fracs.frac2float([1, 2]), 0.5)
        self.assertEqual(fracs.frac2float([1, 4]), 0.25)

    def tearDown(self): pass

def Start_Test():
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFractions)
    unittest.TextTestRunner(verbosity=2).run(suite)