

#-------------------------------------------------
# Zadanie 4.3 - factorial ------------------------
#-------------------------------------------------
def factorial(number):
    iter=1
    factor = 1
    while iter<=number:
        factor*=iter
        iter+=1
    return factor



#-------------------------------------------------
# Zadanie 4.4 - fibbonachi -----------------------
#-------------------------------------------------

def fibbonachi(number):
    fib_0 = 0
    fib_1 = 1
    iter=2
    while iter<=number:
        if(iter%2==0): fib_0 = fib_0+fib_1
        else: fib_1 = fib_0+fib_1
        iter+=1
    if(iter%2==0): return fib_1
    else: return fib_0

