from rekurencja import fibbonachi as fib
from rekurencja import factorial as fac
from os import system
import platform



def Zad_1():
    print "# Factorial\n"
    number = Get_Input()
   
    print "Silnia z " + str(number) + " wynosi: " + str(fac(number))
    system("PAUSE")


def Zad_2(): 
    print "# Fibbonachi\n"
   
    number = Get_Input()
    print str(number) + " liczba fibonacciego: " +str(fib(number))

    system("PAUSE")

#-------------------------------------------------
# Funkcje uniwesalne oraz menu -------------------
#-------------------------------------------------
# Funkcja pobierajaca z klawiatury
def Get_Input():
    number = 0
    while True:
        user=raw_input("Prosze podac liczbe: ")
        if(user.isdigit()):
            number = int(user)
            break
        else: print "Podano bledna wartosc"
    return number

# Funkcja czyszczenia konsoli
def Clear():
    if(platform.system() == "Windows"): system("cls")
    else: system("clear")

# Funkcja menu wyboru
def Menu():
    print "# Zadanie 5.1"
    user = None
    while user!="end":
        Clear()
        print "##### Menu #####"
        print "Prosze wpisac numer zadania i nacisnac enter. \nBy zakonczyc program prosze wpisac [end]."
        print "> 1 - factorial"  
        print "> 2 - fibbonachi" 
        user = raw_input()
        Clear()
        if(user.isdigit()):
            if(int(user) == 1): Zad_1()
            elif(int(user) == 2): Zad_2()
            

Menu()