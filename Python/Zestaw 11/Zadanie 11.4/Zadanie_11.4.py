import time
import ListCreator

def swap(L,p1,p2):
    temp = L[p1]
    L[p1] = L[p2]
    L[p2] = temp


# zwykly bublesort
def bubblesort(L, left, right):
    for i in range(left, right):
        for j in range(left, right):
            if L[j] > L[j+1]:
               swap(L,j, j+1)

# bublesort - Wersja ulepszona wg Sys�y.
def sysbubblesort(L, left, right):
    limit = right
    while True:
        k = left-1   # lewy wska�nik przestawianej pary
        for i in range(left, limit):
            if L[i] > L[i+1]:
                swap(L,i, i+1)
                k = i
        if k > left:
            limit = k
        else:
            break

# Quicksort -  Wersja wg Kernighana i Ritchiego.
def quicksortKR(L, left, right):
    if left >= right:
        return
    swap(L, left, (left + right) / 2)   # element podzia�u
    pivot = left                      # przesu� do L[left]
    for i in range(left + 1, right+1):   # podzia�
        if L[i] < L[left]:
            pivot += 1
            swap(L, pivot, i)
    swap(L, left, pivot)     # odtw�rz element podzia�u
    quicksortKR(L, left, pivot - 1)
    quicksortKR(L, pivot + 1, right)


#Quicksort Wersja wg Cormena.
def quicksortC(L, left, right):
    """Sortowanie szybkie wg Cormena str. 169."""
    if left >= right:
        return
    pivot = partition(L, left, right)
    # pivot jest na swoim miejscu.
    quicksortC(L, left, pivot - 1)
    quicksortC(L, pivot + 1, right)

def partition(L, left, right):
    """Zwraca indeks elementu rozdzielaj�cego."""
    # Element rozdzielaj�cy to ostatni z prawej,
    # dlatego na ko�cu trzeba go przerzuci� do �rodka.
    # B�dzie on na docelowej pozycji ze wzgl�du na sortowanie.
    x = L[right]   # element rozdzielaj�cy
    i = left
    for j in range(left, right):
        if L[j] <= x:
            swap(L, i, j)
            i += 1
    swap(L, i, right)
    return i

def testfunc(func,list):    # funckja testujaca pojedyncze sortowanie 
    
    time1 = time.clock()
    list1 = func(list,0,len(list)-1)
    time1 = time.clock()-time1
    print "\t"+str(func.__name__)+": posortowano liste w czasie: \t" + str(time1) + " s..." 
    return time

def testSortFunc(list_size):    # funkcja testujaca algortmy sortowania
   creator = ListCreator.ListCreator()
   gen_list = creator.gaussFloatList(list_size)
   time_list = list()
#   print "Utworzono liste"

#   time_list.append(testfunc(bubblesort,list(gen_list)))       # odkomentowac by dodac porownanie dla bublesort
#   time_list.append(testfunc(sysbubblesort,list(gen_list)))    # odkomentowac by dodac porownanie dla bublesort sysla
   time_list.append(testfunc(quicksortKR,list(gen_list)))
   time_list.append(testfunc(quicksortC,list(gen_list)))

   return time_list


for i in range(2,7):
    size = pow(10,i);
    print "Dla listy 10^"+str(i)+":"
    testSortFunc(size)
