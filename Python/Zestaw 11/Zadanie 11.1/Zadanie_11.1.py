import random
from math import sqrt

class ListCreator:
    

    def randomIntList(self,upper_val=100, bottom_val =0):  # przypadek a: rozne liczby int od 0 do N-1 w kolejno�ci losowej
        generatedList = list()
        i=0
        numbers = bottom_val
        for i in range(upper_val-bottom_val):   #generaowanie listy 
            generatedList.append(numbers)
            numbers+=1
        i=0
        while(i<upper_val*upper_val):   #zmienianie kolejno�ci w li�cie na losow�
            count1 = random.randint(0,len(generatedList)-1)
            count2 = random.randint(0,len(generatedList)-1)
            temp = generatedList[count1]
            generatedList[count1]=generatedList[count2]
            generatedList[count2]= temp
            i+=1

        return generatedList

    def almostIntList(self,upper_val=100, bottom_val =0):   # przypadek b: rozne liczby int od 0 do N-1 prawie posortowane (liczby s� blisko swojej prawid�owej pozycji)
        generatedList = list()
        i=0
        numbers = bottom_val
        for i in range(upper_val-bottom_val):   #generaowanie listy 
            generatedList.append(numbers)
            numbers+=1
        i=0
        near_section = int(len(generatedList)/10)   # wyznaczanie blisko�ci - przedzia�u w ktorym s� zamieniane miedzy soba liczby
        section_start = 0
        section_end = near_section
        while(True):   #zmienianie kolejno�ci w li�cie na losow�( blisk� swojej pozycji)
            count1 = random.randint(section_start,section_end)
            count2 = random.randint(section_start,section_end)
    
            temp = generatedList[count1]
            generatedList[count1]=generatedList[count2]
            generatedList[count2]= temp

            i+=1
            if(i>near_section*near_section*near_section):
                if(section_end == len(generatedList)-1): break
                i=0
                section_start = section_end
                section_end +=near_section
                if(section_end>len(generatedList)-1): section_end = len(generatedList)-1

        return generatedList

    def revalmIntList(self,upper_val=100, bottom_val =0):  # przypadek c: rozne liczby int od 0 do N-1 prawie posortowane w odwrotnej kolejno�ci
        generatedList = list()

        numbers = upper_val
        while(numbers>=bottom_val):   #generaowanie listy 
            generatedList.append(numbers)
            numbers-=1
        i=0
        near_section = int(len(generatedList)/10)   # wyznaczanie blisko�ci - przedzia�u w ktorym s� zamieniane miedzy soba liczby
        section_start = 0
        section_end = near_section
        while(True):   #zmienianie kolejno�ci w li�cie na losow�( blisk� swojej pozycji)
            count1 = random.randint(section_start,section_end)
            count2 = random.randint(section_start,section_end)
    
            temp = generatedList[count1]
            generatedList[count1]=generatedList[count2]
            generatedList[count2]= temp

            i+=1
            if(i>near_section*near_section*near_section):
                if(section_end == len(generatedList)-1): break
                i=0
                section_start = section_end
                section_end +=near_section
                if(section_end>len(generatedList)-1): section_end = len(generatedList)-1

        return generatedList

    def gaussFloatList(self,val_amount = 100,mean=50, sigma = 0):  # przypadek d: N liczb float w kolejno�ci losowej o rozk�adzie gaussowskim
        generatedList = list()
        if(sigma ==0): sigma = mean/4   # jesli odchylenie nie zosta�o podane, domyslnie ustawiana jest 4 czesc odleglosci srodka przedzialu od 0

        for i in range(val_amount):   #generaowanie listy 
            generatedList.append(random.gauss(mean,sigma))
       

        return generatedList

    def repeatableIntList(self,val_amount=100, bottom_val =0):  # przypadek e: N liczb int w kolejno�ci losowej, o warto�ciach powtarzaj�cych si�, nale��cych do zbioru k elementowego (k < N, np. k*k = N)
        generatedList = list()
        upper_val = int(sqrt(val_amount))

        for i in range(val_amount):
            generatedList.append(random.randint(bottom_val,upper_val))

        return generatedList


''' Odkomentowac by przetestowac

import time
def functest(function, val_1,val2):

    start = time.clock()
    list = function(val_1,val2);
    end = time.clock()
    print str(function.__name__)+":\nList: " + str(list) + "\n\t in " + str(end-start)


generator = ListCreator()

functest(generator.randomIntList,50,0)
functest(generator.almostIntList,50,0)
functest(generator.revalmIntList,50,0)
functest(generator.gaussFloatList,50,10)
functest(generator.repeatableIntList,50,0)

'''