


# Zadanie 2.10
print "# ZADANIE 2.10"
line = " To jest napis, ktory ma \n wiele wierszy i jest dlugi. \n A tu jest 3 linija. \n A tu jest 4 linijka."
print line + "\n\n Napis posiada: " + str(len(line.split()))+" wyrazow.\n"

# Zadanie 2.11
print "\n# ZADANIE 2.11"
word = " Slowo"
string = ""
for symbol in word:
    string+= symbol+" "
print string


# Zadanie 2.12
print "\n# ZADANIE 2.12"
line = " TasdaM AdasaA TsdasM AasdaA\n"
print " " + line
string_first = ""
string_last = ""
for word in line.split():
    string_first+= word[0]
    string_last+= word[len(word)-1]

print " String z pierwszych liter: " + string_first
print " String z ostatnich liter: " +string_last

# Zadanie 2.13
print "\n# ZADANIE 2.13"

line_length = 0
for word in line.split():
    line_length+=len(word)
print  " Ilosc czarnych znakow: " + str(line_length) + " z: "+ str(len(line))

# Zadanie 2.14
print "\n# ZADANIE 2.14"

line = " AA BB CCC D E onomatopeja gg ee"
print " " + line
line_length = 0
max_word = ""
for word in line.split():
    if(line_length<len(word)): 
        line_length = len(word)
        max_word = word

print " Najdluzsze slowo: " + max_word + " ( "+str(line_length) + " znakow )"

# Zadanie 2.15
print "\n# ZADANIE 2.15"
L=["12","14","66","6"]
print L
word= ""
for num in L:
    word+=num
print " Ciag liczb: " + word

# Zadanie 2.16
print "\n# ZADANIE 2.16"

line = " ggg GGG GVR GvR qvR gvr klnkasd klsjdjd askks GvR"
print line
print "  ||"
new_line = " "
for word in line.split():
    if(word!= "GvR"): new_line = new_line + word + " "
    else: new_line = new_line + "Guido van Rossum" + " "
line = new_line
print line

# Zadanie 2.17
print "\n# ZADANIE 2.17"

line = " C G A P www DDD aaa DJKF JKSNJKDS MJA sjdakj sjsmaaakd"
line_list = line.split()
print " Sortowanie alfabetyczne: " 
print sorted(line_list)
print " Sortowanie dlugoscia: " 
print sorted(line_list,key= len)

# Zadanie 2.18
print "\n# ZADANIE 2.18"

big_number = 10560201201020302
zero_counter = 0
for num in str(big_number): 
    if(num == '0'): zero_counter+=1
print " W liczbie: "+ str(big_number) + " znajduje sie: " + str(zero_counter) + " zer."

# Zadanie 2.19
print "\n# ZADANIE 2.19"

L = ["7","22","333","42","666","1","87"]
iter = 0
while iter<len(L):
    L[iter] = L[iter].zfill(3)
    iter+=1
print L
