#!/usr/bin/env python
# -*- coding: utf-8 -*- 

import MDisplay
import MSystem
import MInput
import MLexicon

class T9:
    ''' Glowna klasa programu. Zbiera poszczegolne moduly i wykorzystuje je. 
    Zawiera takze funkcje odpowiedzalne za przekladanie z odpowienich wejsc na litery'''


    def __init__(self,file_path="slownik.txt"):
        ''' Inicjalizacja calej klasy, aktywuje poszczegolne moduly i wyciaga z nich potrzebne zmienne'''
        self.file_input = MInput.File(file_path)
        self.platform = MSystem.Platform()
        self.display = MDisplay.Display(self.platform.cursor,80,25)
        self.lex = MLexicon.Lexicon(self.file_input,self.display.Print_Comm)
        self.keyboard = MInput.Keyboard(self.platform.keyboard,self.platform.await)


        

    
    def NumToChar(self,key,counter):
        ''' Zamiana z liczby wprowadzonej na odpowiednia litere (zaleznie od ilosci wcisniec)'''
        if(key == '7') or (key=='9'):
            div = 4
        else: 
            div=3
        counter-=1
        counter = counter%div

        if(key == '2'):
            if(counter==0):
                return 'a'
            elif(counter==1):
                return 'b'
            elif(counter==2):
                return 'c'
        elif(key == '3'):
            if(counter==0):
                return 'd'
            elif(counter==1):
                return 'e'
            elif(counter==2):
                return 'f'
        elif(key == '4'):
            if(counter==0):
                return 'g'
            elif(counter==1):
                return 'h'
            elif(counter==2):
                return 'i'
        elif(key == '5'):
            if(counter==0):
                return 'j'
            elif(counter==1):
                return 'k'
            elif(counter==2):
                return 'l'
        elif(key == '6'):
            if(counter==0):
                return 'm'
            elif(counter==1):
                return 'n'
            elif(counter==2):
                return 'o'
        elif(key == '7'):
            if(counter==0):
                return 'p'
            elif(counter==1):
                return 'q'
            elif(counter==2):
                return 'r'
            elif(counter==3):
                return 's'
        elif(key == '8'):
            if(counter==0):
                return 't'
            elif(counter==1):
                return 'u'
            elif(counter==2):
                return 'v'
        elif(key == '9'):
            if(counter==0):
                return 'w'
            elif(counter==1):
                return 'x'
            elif(counter==2):
                return 'y'
            elif(counter==3):
                return 'z'

                    
   
    def start(self):
        ''' Metoda rozpoczynajaca dzialanie programu'''
        string = ""
        key = None
        position = 0
        load = True
        while True:
            key, num = self.keyboard.Get_Input() 
            if(key=='1'):   # Obsluga zakanczania przy wcisniecu 1
                break
            key = self.NumToChar(key, num)
            if(key!=None): 
                string+=key
            self.display.Print_Word(string)
            position+=1
            if(len(self.lex.lex)<=0) and (load):
                self.lex.Load_Lexicon(key)
                load = False
            else:
                self.lex.Search(key,position)
            self.display.Print_Results(self.lex.lex)
            




dict = T9()
dict.start()