#!/usr/bin/env python
# -*- coding: utf-8 -*- 


class Display:
    ''' Klasa zajmujaca sie wyswietlaniem UI oraz kontrola wyswietlanych tekstow '''
     

    def __init__(self,cursor,widith,height,column_number=3,decode_standard = "utf-8" ):
        ''' Ustawia potrzebne programowi zmienne wynikajace ze wzglednej pozycji elementow'''
        self.decode_standard = decode_standard
        self.cursor = cursor    # kursor do poruszania sie po obszarze ramki
        self.widith = widith    # szerokosc calej ramki
        self.height = height+1    # wysokosc calej ramki
        self.inner_height = height-4    # wysokosc kolumn wewnatrz ramki
        self.comm = self.inner_height+4    # pozycja wiersza komunikatow
        self.keypad = self.inner_height+2  # pozycja wiersza klawiatury
        self.column_widith = widith/column_number  # szerokosc pojedynczej kolumny
        self.column_number = column_number
        self.Generate_UI()
        

    
    def Console_Clean(self,x,y,length):
        '''Usuwa okreslona liczbe znakow poczawszy od zadanej pozycji'''
        self.cursor(x,y)
        
        string = ""
        for i in range(length):
            string+=" "
        print string
    
    def Print_Word(self,word):
        '''Wypisuje slowo, ktore wpisywane jest z klawiatury'''
        self.Console_Clean(2,self.comm,10)
        self.cursor(2,self.comm)
        print word.decode(self.decode_standard, 'ignore')
        self.Cursor_End()
                
   
    def Print_Comm(self,text):
        '''Wypisuje podany tekst w polu komunikatu'''
        self.Console_Clean(12,self.comm,self.widith-13)
        self.cursor(12,self.comm)
        print"| "+ text.decode(self.decode_standard, 'ignore')
        self.Cursor_End()
    
    
    def Print_KeyPad(self):
        '''Wypisywanie przelozenia liter na liczby'''
        x_add = (self.widith-2)/9
        x_pos = (self.widith+2- x_add*9)/2
        letter = ord('a')
        for i in range(1,10):
            
            if(i==1):
                string = str(i) + "-END "
            elif(i==7) or(i==9):
                string = str(i)+"-"+ chr(letter) + chr(letter+1)+chr(letter+2)+chr(letter+3)
                letter+=4
            else:
                string = str(i)+"-"+ chr(letter) + chr(letter+1)+chr(letter+2)+" "
                letter+=3
            self.cursor(x_pos,self.keypad)
            print string
            x_pos+=x_add
        



    
    def Print_In_Column(self,text,column_num,y):
        '''Wypisuje tekst w podanej kolumnie i na odpowiedniej wysokosci'''

        # Ustawianie x pozycji dla kursora zaleznie od kolumny
        if(column_num==1): column_num = 2     
        elif(column_num==2): column_num = 1 + self.column_widith+1
        elif(column_num==3): column_num = 2 + 2*(self.column_widith) 

        if(len(text)>= self.column_widith): 
            text = text[0:self.column_widith-4]+"..." # Ukrywa reszte tekstu gdy jest on zbyt dlugi na kolumne

        self.cursor(column_num,y)
        print text.decode(self.decode_standard, 'ignore')
        if(self.column_widith-4-len(text)>0):
            self.Console_Clean(column_num+len(text),y,self.column_widith-4-len(text))

    def Cursor_End(self):
        '''Ustawia kursor poza obrazem UI'''
        self.cursor(0,self.height+1)

    
    def Generate_UI(self):
        '''Generowanie interfejsu uzytkownika'''
        
        border = Border()
        border.Print_Outside_Border(self.widith, self.inner_height)
        border.Print_Inner_Border(self.cursor,self.widith,self.inner_height,self.column_number)
        self.Print_KeyPad()
        self.Cursor_End()

    def Print_Results(self,lex):
        '''Wypisywanie resultatów wyszukiwania na klawiaturze'''
        i=0
        j=0
        result_counter=0
        for word in lex:
            self.Print_In_Column(str(word),i+1,j+1)
            result_counter+=1
            j+=1
            if(j>=self.inner_height):
                j=0
                i+=1
            if(i>=self.column_number):
                break
        if(i!=self.column_number) or (j!=self.inner_height):
            for col in range(i,self.column_number):
                for row in range(0,self.inner_height):
                    if(col ==i) and (row<j):
                        continue
                    self.Print_In_Column("",col+1,row+1)
        self.Cursor_End()

            
class Border:
    ''' Klasa zawierajaca metody tworzenia ramek i ich skladowe '''



    '''Znaki ramki - zawieraja wszelkie znaki uzyte do generowania ramek interfejsow '''
   
    # Zmienne wyswietlajace (znaki ramki)
    M_LUCOR_B= '\u2557'	# Znak lewy gorny rog ramki
    M_RUCOR_B= '\u2554'	# Znak prawy gorny rog ramki
    M_HOR_B = '\u2550'	# Znak pozioma krawedz ramki
    M_VER_B = '\u2551'	# Znak pionowa krawedz ramki
    M_LLCOR_B= '\u255D'	# Znak lewy dolny rog ramki
    M_RLCOR_B= '\u255A'	# Znak prawy dolny rog ramki
    M_LMCOR_B= '\u2563'	# Znak lewy srodkowy rog ramki
    M_RMCOR_B= '\u2560'	# Znak prawy srodkowy rog ramki
    M_IVER_B = '\u2502' # Znak wewnetrzna krawedz pionowa
    M_UHOR_B = '\u2560' # Znak gorne laczenie krawedzi wewnetrznej
    M_LHOR_B = '\u2567' # Znak dolne laczenie krawedzi wewnetrznej


   
    def P_US_Border(self,widith):
        '''Tworzenie gornej czesci ramki'''
        border_string = self.M_RUCOR_B.decode('unicode-escape')  
        for i in range(widith):
            border_string+=self.M_HOR_B.decode('unicode-escape') 
        border_string+=self.M_LUCOR_B.decode('unicode-escape')   
        print border_string

    def P_LS_Border(self,widith):
        '''Tworzenie dolnej czci ramki'''
        border_string = self.M_RLCOR_B.decode('unicode-escape')
        for i in range(widith):
            border_string+=self.M_HOR_B.decode('unicode-escape')
        border_string+=self.M_LLCOR_B.decode('unicode-escape')
        print border_string

    def P_MS_Border(self,widith):
        ''' Tworzenie srodkowej czesci ramki (separator)'''
        border_string = self.M_RMCOR_B.decode('unicode-escape')
        for i in range(widith):
            border_string+=self.M_HOR_B.decode('unicode-escape')
        border_string+=self.M_LMCOR_B.decode('unicode-escape')
        print border_string

   
    def P_ES_Border(self,widith):
        '''Tworzenie srodkowej czesci ramki (obramowanie)'''
        border_string = self.M_VER_B.decode('unicode-escape')
        for i in range(widith):
            border_string+=" "
        border_string+=self.M_VER_B.decode('unicode-escape')
        print border_string

    def Print_Outside_Border(self,widith, inner_height):
        '''Tworzenie zewnetrznej ramki'''
        widith-=2   # Odejmowanie szerokosci prawej i lewej strony ramki 

        self.P_US_Border(widith)
        
        for i in range(inner_height):
            self.P_ES_Border(widith)
        self.P_MS_Border(widith)
        self.P_ES_Border(widith)
        self.P_MS_Border(widith)
        self.P_ES_Border(widith)
       
        self.P_LS_Border(widith)

    
    def Print_Inner_Border(self,Cursor, widith, height,column_number):
        ''' Tworzenie wewnetrznej ramki'''
        widith = widith/column_number
        for col in range(1,column_number):
            for i in range(height):
                Cursor(col*widith,i+1)
                print self.M_IVER_B.decode('unicode-escape')
                