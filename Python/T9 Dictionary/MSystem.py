#!/usr/bin/env python
# -*- coding: utf-8 -*- 
from ctypes import * # Importowanie potrzebnych dla windows bibliotek
import platform
if(platform.system() =="Windows"):
    import msvcrt
else:
   import sys
   import termios
   import atexit
   from select import select

    


class Platform:
    ''' Sluży do obslugi roznic przy innych systemach operacyjnych'''
    def __init__(self):
        '''Ustala na jakiej platformie pracuje i ustawia funkcje zaleznie od platformy'''
        self.platform = platform.system()  # Okreslenie srodowiska, na ktorym pracuje program
        self.Set_Controlers()
       
        

    
    def Set_Controlers(self):
        '''Ustalanie funkcji obslugujacej kontrolery konsoli'''
        if(self.platform == "Windows"):
            
            self.cursor =  self.Win_Cursor
            self.keyboard = self.Win_Keyboard
            self.await = self.Win_Key_Await
        else:
            from os import system as ossystem
            ossystem("clear")   # czyszczenie konsoli
            self.shell = KBHit()
            self.cursor =  self.Lin_Cursor
            self.keyboard = self.Lin_Keyboard
            self.await = self.Lin_Key_Await
            

    # Obsluga kursora konsoli dla sysemu Windows
    def Win_Cursor(self,x,y):
        STD_OUTPUT_HANDLE = -11
        class COORD(Structure):
             pass
        COORD._fields_ = [("X", c_short), ("Y", c_short)]
        windll.kernel32.SetConsoleCursorPosition(windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE), COORD(x, y))

    # Obsluga kursora konsoli dla sysemu Linuxa
    def Lin_Cursor(self,x,y):
        print("\033["+str(y+1)+";"+str(x)+"H"),


    # Obsluga poboru z klawiatury dla Widnows
    def Win_Keyboard(self):
        return msvcrt.getch().upper()

    # Oczekiwanie na wcisniecie klawisza dla Windows
    def Win_Key_Await(self):
        return msvcrt.kbhit()

    # Obsluga poboru z klawiatury dla Linux
    def Lin_Keyboard(self):
        return self.shell.getch()
    
    # Oczekiwanie na wcisniecie klawisza dla Linux
    def Lin_Key_Await(self):
        return self.shell.kbhit()
       


class KBHit:
    ''' Klasa obslugujaca nieblokowalne wejscie z klawiatury dla linuxa'''
    
    def __init__(self):  
        '''Tworzy obiekt, ktoy mozna wykorzystac do zbierania wejscia z klawiatury na linuxe'''
        # Zapisywanie ustawien terminalu
        self.fd = sys.stdin.fileno()
        self.new_term = termios.tcgetattr(self.fd)
        self.old_term = termios.tcgetattr(self.fd)
        
        # Ustawianie nowych wlasciwosci terminalu 
        self.new_term[3] = (self.new_term[3] & ~termios.ICANON & ~termios.ECHO)
        termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.new_term)
    
        # Przywaraca poczatkowe ustawienia teminalu podczas konczenia
        atexit.register(self.set_normal_term)
    
    
    def set_normal_term(self):
        ''' Przywaraca podstawowe ustawienia terminala'''
        termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.old_term)


    def getch(self):
        ''' Zwraca klawisz wcisniety z klawiatury   '''
        return sys.stdin.read(1)

    def kbhit(self):
        ''' Zwraca True jezeli klawisz zostal wcisniety, inaczej False'''
        dr,dw,de = select([sys.stdin], [], [], 0)
        return dr != []