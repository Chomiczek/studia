import time

class Lexicon:

    def __init__(self,file,message_sender):
        self.file = file
        self.send_Message = message_sender
        self.lex = set()


    def Load_Lexicon(self,letter):
        '''Wczytywanie zasobu slow dla podanej litery'''
        search_time = time.clock()
        self.file.Open_File()
        self.send_Message("Rozpoczynam wczytywanie slownika . . .")
        word = ""
        while True:
            word = self.file.Get_Line()
            if(word=="") or (word==None): break     # Jezli koniec pliku - zakoncz wczytywanie
            if(word[0]!=letter) and (len(self.lex)>0): break # Ulozenie alfabetyczne zapewnia, ze nie ma potrzeby sprawdzaa rekordow zaczynajacych sie na kolejne (po szukanej) litery
            if(word[0]==letter): self.lex.add(word)
        self.send_Message("Wczytano "+ str(len(self.lex)) + " w czasie " + str(time.clock()-search_time) + " s . . .")
        self.file.Close_File()

    def Find_Notification(self, number_of_records, search_time):
        ''' Powiadomienie o liczbie i czasie znalezionych rekordow'''
        self.send_Message("Znaleziono "+ str(number_of_records) + " w czasie " + str(search_time) + " s . . .")
    
    def Search(self,letter, letter_position):
        '''Wyszukiwanie slow pasujacych do podanego klucza'''
        start_time = time.clock()
        discard_lex = set()
        for word in self.lex:
            if(len(word)<= letter_position-1) or (word[letter_position-1]!=letter):
                discard_lex.add(word)
        for word in discard_lex:
            self.lex.remove(word)
        self.Find_Notification(len(self.lex),time.clock()-start_time)

  