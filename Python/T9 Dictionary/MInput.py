
class File:
    ''' Klasa zajmuje sie obsluga plikow zewetrznych, przeznaczona do stosowania z plikami txt'''


    def  __init__(self,path,mode="r"):
        self.path = path
        self.mode = mode
        

    def Open_File(self):
        self.file = open(self.path,self.mode)
        return self.file

    def Close_File(self):
        self.file.close()

    def Get_Line(self):
        return self.file.readline()

import time
class Keyboard:
    ''' Klasa obslugujaca wejscie z klawiatury'''
    def __init__(self,keyboard,await):
        self.keyboard_input = keyboard
        self.await_input = await

    
    def Get_Input(self):
        '''Pobieranie klawisza z klawiatury, liczy liczbe klikniec '''
        input_key = None
        privous_key = None
        key_counter = 0
        time_left = None
        countdown = False
        while True:
            if(self.await_input()):
               input_key = self.keyboard_input()
               countdown=True
               if(privous_key==None):
                   time_left = time.clock()+0.6 
                   privous_key = input_key;
               if(privous_key == input_key):
                   key_counter+=1
                   time_left=time.clock()+0.6
               else: 
                   break
            if (countdown) and (time_left-time.clock()<0):
                break
           
        return privous_key,key_counter


