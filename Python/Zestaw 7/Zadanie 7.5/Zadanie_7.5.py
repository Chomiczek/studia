from points import Point
import math

class Circle:
    """Klasa reprezentuj�ca okr�gi na p�aszczy�nie."""

    def __init__(self, x, y, radius):
        if radius < 0:
            raise ValueError("promie� ujemny")
        self.pt = Point(x, y)
        self.radius = radius

    def __str__(self):
        return "["+str(self.pt)+", r= "+ str(self.radius)+"]"

    def __repr__(self):        # "Circle(x, y, radius)"
        return "Circle("+self.pt[0] +", "+self.pt[1]+", "+self.radius+")"

    def __eq__(self, other):
        return self.pt == other.pt and self.radius == other.radius

    def __ne__(self, other):
        return not self == other

    def area(self):            # pole powierzchni
        return math.pi*self.radius*self.radius 

    def move(self, x, y):     # przesuniecie o (x, y)
        return Circle(self.pt.x+x,self.pt.y+y,self.radius)

    def cover(self, other):    # okr�g pokrywaj�cy oba
        if(self==other): return self
        if(self.radius>other.radius): new_radius = self.radius
        else: new_radius = other.radius
        if(self.pt == other.pt): return Circle(self.pt.x,self.pt.y,new_radius)
        new_radius+=math.sqrt(pow(self.pt.x-other.pt.x,2)+pow(self.pt.y-other.pt.y,2))
        return Circle((self.pt.x+other.pt.x)/2,(self.pt.y+other.pt.y)/2,new_radius)


# Kod testuj�cy modu�.

import unittest

class TestCircle(unittest.TestCase): 

    def test_eq(self):
        self.assertTrue(Circle(1,2,3)==Circle(1,2,3))
        self.assertTrue(Circle(-1,2,0)==Circle(-1,2,0))
        self.assertFalse(Circle(1,2,2)==Circle(1,2,3))
        self.assertTrue(Circle(1,2,2)!=Circle(1,2,3))
    
    def test_area(self):
        self.assertEqual(Circle(1,2,3).area(),math.pi*9)
        self.assertEqual(Circle(2,2,4).area(),math.pi*16)
        self.assertEqual(Circle(1,2,1).area(),math.pi*1)
        self.assertEqual(Circle(1,2,0).area(),math.pi*0)


    def text_move(self):
        self.assertTrue(Circle(0,0,3).move(1,2)==Circle(1,2,3))
        self.assertTrue(Circle(0,0,3).move(-1,2)==Circle(-1,2,3))
        self.assertTrue(Circle(0,0,6).move(1,2)==Circle(1,2,6))

    def test_cover(self):
        self.assertTrue(Circle(0,0,3).cover(Circle(0,0,2))==Circle(0,0,3))
        self.assertTrue(Circle(1,1,3).cover(Circle(0,0,2))==Circle(0,0,3+math.sqrt(2)))


# funkcja pozwalajaca rozpoczac testy z poziomu programu
def Start_Test():
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCircle)
    unittest.TextTestRunner(verbosity=2).run(suite)



Start_Test()
#if __name__ == '__main__': unittest.main()
