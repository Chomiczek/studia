

class Point:

    def __init__(self,x,y):
        self.x = x
        self.y = y

    def __str__(self):
        return "Point("+str(self.x)+", "+str(self.y)+")"

    def __eq__(self,other):
        if(self.x == other.x) and (self.y == other.y): return True
        else: return False

    def __ne__(self, other):
        return not self == other

    def __add__(self,other):
        return Point(self.x+other.x,self.y+other.y)

    def __sub__(self,other):
        return Point(self.x-other.x,self.y-other.y)

