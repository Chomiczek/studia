
class DenomError(ValueError):
    def __init__(self):  pass   
        

    def __str__(self):            
        return "Podano bledna wartosc mianownika. Nie moze byc 0!!!"





class Frac:
    """Klasa reprezentująca ułamki."""

    def __init__(self, x=0, y=1):
        if(y==0): raise DenomError()
        simplify = self.nwd(x,y)
        self.count = x/simplify
        self.denom = y/simplify

    def nwd(self,num1,num2):
        rem=None
        while num2!=0:
            rem = num1%num2
            num1=num2
            num2= rem
        return num1

    def __str__(self):          # zwraca "x/y" lub "x" dla y=1
        if(self.denom==1): return str(self.count)
        else: return  str(self.count)+"/"+str(self.denom)

    def __repr__(self):         # zwraca "Frac(x, y)"
        return  "Frac("+str(self.count)+", "+str(self.denom)+")"
    
    def __cmp__(self, other):   # porównywanie
            if(self.__float__() >other.__float__()): return -1
            elif(self.__float__() == other.__float__()): return 0
            else: return 1

    def __add__(self, other):   # frac1+frac2, frac+int
        if(isinstance(other,(int,long))): return Frac(self.count+(other*self.denom),self.denom) 
        if (self.denom != other.denom):
            return Frac(self.count * other.denom + other.count * self.denom,self.denom * other.denom)
        else:
            return Frac(self.count + other.count,self.denom)

    __radd__ = __add__              # int+frac

    def __sub__(self, other):   # frac1-frac2, frac-int
        if(isinstance(other,(int,long))): return Frac(self.count-(other*self.denom),self.denom) 
        if (self.denom != other.denom):
            return Frac(self.count * other.denom - other.count * self.denom,self.denom * other.denom)
        else:
            return Frac(self.count - other.count,self.denom)

    def __rsub__(self, other):      # int-frac
        # tutaj self jest frac, a other jest int!
        return Frac(self.denom * other - self.count, self.denom)

    def __mul__(self, other):   # frac1*frac2, frac*int
        if(isinstance(self,(int,long))): return Frac(self*other.count,other.denom)
        elif(isinstance(other,(int,long))): return Frac(self.count*other,self.denom)
        else: return Frac(self.count * other.count,self.denom * other.denom)

    __rmul__ = __mul__              # int*frac

    def __div__(self, other):   # frac1/frac2, frac/int
        if(isinstance(other,(int,long))):
           if(other==0): raise DenomError()
           else: return Frac(self.count,self.denom*other)
        if(other.denom == 0): raise DenomError()
        else: return Frac(self.count * other.denom,self.denom * other.count)

    def __rdiv__(self, other): # int/frac
        if(self.count == 0) or (self.denom == 0): raise DenomError()
        return Frac(other*self.denom, self.count)


    # operatory jednoargumentowe
    def __pos__(self):  # +frac = (+1)*frac
        return self

    def __neg__(self):          # -frac = (-1)*frac
        return Frac(-self.count, self.denom)

    def __invert__(self):       # odwrotnosc: ~frac
        return Frac(self.denom, self.count)

    def __float__(self):        # float(frac)
        return float(self.count)/float(self.denom)

# Kod testujący moduł.
import unittest


class TestFrac(unittest.TestCase):

    def test_zero(self):
        try: Frac(2,0)
        except Exception as exception: self.assertEqual(exception.__class__, DenomError)

    def test_add(self):
        self.assertTrue(Frac(1,2)+Frac(1, 3)== Frac(5, 6))
        self.assertEqual(Frac(1,-2)+Frac(1, 3), Frac(-1, 6))
        self.assertEqual(Frac(1,2)+1, Frac(3, 2))
        self.assertEqual(1+(Frac(1,2)), Frac(3, 2))

    def test_sub(self): 
        self.assertEqual(Frac(1, 2)-(Frac(1, 4)), Frac(1, 4))
        self.assertEqual(Frac(1, 2)-2, Frac(-3, 2))
        self.assertEqual(2-Frac(1, 2), Frac(3, 2))

    def test_mul(self): 
        self.assertEqual(Frac(1, 2)*(Frac(1, 3)), Frac(1, 6))
        self.assertEqual(Frac(1, 2)*2, Frac(1, 1))
        self.assertEqual(2*Frac(1, 3), Frac(2, 3))

    def test_div_frac(self): 
        self.assertEqual(Frac(1, 2)/(Frac(1, 3)), Frac(3, 2))
        self.assertEqual(Frac(1, 2)/2, Frac(1, 4))
        self.assertEqual(2/(Frac(1, 3)), Frac(6, 1))
    
    def test_cmp(self): 
         self.assertEqual(Frac(1, 2).__cmp__(Frac(1, 3)), -1)
         self.assertEqual(Frac(1, 3).__cmp__(Frac(1, 2)), 1)
         self.assertEqual(Frac(1, 2).__cmp__(Frac(1, 2)), 0)
         self.assertEqual(Frac(1, -3).__cmp__(Frac(1, -2)), -1)

    def test_float(self): 
        self.assertEqual(Frac(1, 2).__float__(), 0.5)
        self.assertEqual(Frac(1, 4).__float__(), 0.25)

    def test_invert(self): 
        self.assertEqual(Frac(1, 2).__invert__(), Frac(2,1))
   
    def test_neg(self): 
        self.assertEqual(Frac(1, 2).__neg__(), Frac(-1,2))


def Start_Test():
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFrac)
    unittest.TextTestRunner(verbosity=2).run(suite)



#Start_Test()

if __name__ == '__main__': unittest.main()