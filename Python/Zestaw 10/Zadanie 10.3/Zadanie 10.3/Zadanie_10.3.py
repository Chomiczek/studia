
class Stack:
    stacktable = []
    contains = []

    def __init__(self,size):
        for i in range(size):
            self.contains.append(0)

    def __str__(self):
        string = "[ "
        for var in self.stacktable:
            string=string + str(var)+", "
        string+="]"
        return string

    def pop(self):
        length = len(self.stacktable)-1
        if(length == -1): raise ValueError("Brak elementow w stosie")
        temp = self.stacktable[length]
        self.stacktable.remove(temp)
        self.contains[temp] = 0
        return temp

    def push(self,var):
        if(var> len(self.contains)): raise ValueError("Podany argument wykracza poza ustalony zakres liczb")
        if(self.contains[var]==0):
            self.stacktable.append(var)
            self.contains[var] = 1
            return True
        else: return False


''' Odkomentowac by sprawdzic dzialanie

s = Stack(14)
s.push(2)
s.push(4)
s.push(6)
s.push(4)
s.push(4)
s.push(5)
s.push(12)
s.push(10)
try:
    s.push(40)
except ValueError as e: print "Wylapano wyjatek: " + e.message

print s

print str(s.pop())


for i in range(len(s.stacktable)):
    s.pop()

print s

'''