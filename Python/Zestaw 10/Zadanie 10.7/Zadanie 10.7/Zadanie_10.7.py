
class PriorityQueue:

    def __init__(self, cmpfunc=cmp):
        self.items = []
        self.cmpfunc = cmpfunc

    def __str__(self):   # podgl�damy kolejk�
        return str(self.items)

    def is_empty(self):
        return not self.items

    def insert(self, item):
        self.items.append(item)

    def remove(self):
        maxi = 0
        for i in range(1, len(self.items)):
            if (self.cmpfunc(self.items[i], self.items[maxi])==1): maxi = i # korzystając z porównania szukamy największy element
        return self.items.pop(maxi)   # ma�o wydajne

    def cmp(self,element1,element2):
        if(element1>element2): return 1
        elif(element1==element2): return 0
        else: return -1


