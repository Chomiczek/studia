import random
import time

class RandomQueue:

    def __init__(self):
        self.items = []
        

    def insert(self, item):
        self.items.append(item)
        for i in range(len(self.items)-1):
            v1 = random.randint(0,len(self.items)-1)
            v2 = random.randint(0,len(self.items)-1)  
            temp = self.items[v1]
            self.items[v1] = self.items[v2]
            self.items[v2] = temp

    def remove(self):
        return self.items.pop(0)

    def is_empty(self):
        if(len(self.items) == 0): return True
        else: return False


    def is_full(self):
        return False


rq = RandomQueue()

for i in range(20):
    rq.insert(i)


time_table = list()
for i in range(19):
    time1 = time.clock()
    rq.remove()
    time1 = time1- time.clock()
    time_table.append(time1)

max = -1
for t in time_table:
    if(max<t): max = t
for t in time_table:
        print(str(max-t)) 

    