#include "MainHeader.h"
#include <iostream>
#include <chrono>
#include "IO.cpp"
#include "Binarisation.cpp"
#include "Comparer.cpp"


class Main {

private:
	
	
	
	static bool checkName(string name)
	{
		if (name.find(".png") == string::npos) return false;
		return true;
	}

	/**
		Getting file name and loading file

		@param image	table for loaded image
	*/
	static void getFile(Image* img)
	{
		bool isValid = false;
		string fileName = "image.png";
		do {
			cout<<"# Podaj nazw� pliku do wczytania: \n";
			cin >> fileName;
			isValid = checkName(fileName);
			if (!isValid) cout << "# Podana nazwa jest nieprawid�owa. \n# Podaj nazw� wraz z rozszerzeniem '.png'.\n";
		} while (!isValid);

		if (!IO::loadImage(img, fileName))
		{
			cout<<"# Nie znaleziono pliku o podanej nazwie. . . \n# Program zostanie zamkni�ty . . . \n";
			system("Pause");
			exit(1);
		}
		
	}

	static void interpretResults(double c)
	{
		cout << "# Por�wnanie: \n";
		string text = "";
		if (c > 0.85) text = " Perfekcyjne pismo!";
		else if (c > 0.60) text = " Wysoka zgodno�� liter";
		else if (c > 0.50) text = " Dobra zgodno�� liter";
		else if (c > 0.40) text = " Poprawna zgodno�� liter";
		else if (c > 0.20) text = " Marna zgodno�� liter";
		else if (c < 0.20) text = " Litery nie przypominaj� siebie!";
		cout << "\t"<< text<<" \n";
		cout << "\t Zgodno�� poziomu:  " << c * 100 << "%\n";

	}

public:
	static void start()
	{

		setlocale(LC_ALL, "Polish");
		cout << "Rozpoznawanie pisma by Izabela Musztyfaga\nWersja: 0.70\n\n";
		Image *img = new Image();

		chrono::high_resolution_clock::time_point t1 = chrono::high_resolution_clock::now();
		getFile(img);
		chrono::high_resolution_clock::time_point t2 = chrono::high_resolution_clock::now();
		chrono::duration<double> time_span = chrono::duration_cast<chrono::milliseconds>(t2 - t1);
		cout << "# Obraz za�adowano w czasie: " << time_span.count()<< " ms . . .\n";

		t1 = chrono::high_resolution_clock::now();
		
		
		Binarisation::covertToGreyscale(img);
		Binarisation::Bradley(img,true,15,0.3);
		t2 = chrono::high_resolution_clock::now();
		
		time_span = chrono::duration_cast<chrono::milliseconds>(t2 - t1);
		cout << "# Binaryzacja zako�czona w czasie: " << time_span.count() << " ms . . .\n";

		t1 = chrono::high_resolution_clock::now();
		double comp = Comparer::Compare(img);
		t2 = chrono::high_resolution_clock::now();
		time_span = chrono::duration_cast<chrono::milliseconds>(t2 - t1);
		cout << "# Por�wnywanie zako�czone w czasie: " << time_span.count() << " ms . . .\n";

		interpretResults(comp);


		IO::saveImage(img,"Zgodnosc.png");
	
		
		
	}
};


int main()
{
	
	Main::start();
	system("Pause");
	return 0;
}