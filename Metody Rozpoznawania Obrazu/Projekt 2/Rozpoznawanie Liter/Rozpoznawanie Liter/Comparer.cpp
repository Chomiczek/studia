#include "MainHeader.h"
#include <vector>
#include <math.h>

/**
	Contains all methods needed for 
	letters (shapes) comparison in given image
*/
class Comparer
{
private:
	
	/**
		Returns center of shape (letter)
	*/
	static int* GetCenter(vector<int> x, vector<int> y)
	{
		int center_x = 0;
		int center_y = 0;
		for (int i = 0; i < x.size(); i++)
		{
			center_x += x[i];
			center_y += y[i];
		}
		center_x /= x.size();
		center_y /= y.size();

		return new int[2]{ center_x,center_y };
	}


	/**
		Returns point distans from center
	*/
	static bool rightDistance(int center[], int point[],int radius)
	{
		if (radius == abs(center[0] - point[0]) + abs(center[1] - point[1])) return true;
		else return false;

	}

	/**
		Checking if given radius is in image (from both centers)
	*/
	static bool inBounds(Image* img, int radius, int center1[], int center2[])
	{
		int max_h = img->height- 0.1 * img->height;
		int max_w = img->width - 0.1 * img->width;
		if (center1[0] - radius < 0 || center1[1] - radius < 0 || center2[0] - radius < 0 || center2[1] - radius < 0) return false;
		if (center1[0] + radius >= max_h || center2[0] + radius >= max_h) return false;
		if (center1[1] + radius >= max_w || center2[1] + radius >= max_w) return false;
		return true;

	}

	/**
		Checking if point is inside image
	*/
	static bool inBounds(Image* img, int point[])
	{
		if (point[0] >= img->height || point[1] >= img->width) return false;
		if (point[0] <= 0 || point[1] <=0) return false;
		return true;
	}

	static void Check(Image* img, int center1[], int center2[])
	{
		int radius = 0;
		int* point1 = new int[2];
		int* point2 = new int[2];
		png_bytep px1;
		png_bytep px2;
		while (inBounds(img,radius/2,center1,center2) )
		{


			for (int i = -radius; i <= radius; i++)
			{
				for (int j = -radius; j <= radius; j++)
				{
					point1[0] = center1[0] + i; point1[1] = center1[1] + j;
					point2[0] = center2[0] + i; point2[1] = center2[1] + j;
					if (!inBounds(img, point1)) continue;
					if (!inBounds(img, point2)) continue;
					if (rightDistance(center1, point1, radius) && rightDistance(center2, point2, radius))
					{
						
						px1 = &(img->imageTable[point1[0]][point1[1] * 4]);
						px2 = &(img->imageTable[point2[0]][point2[1] * 4]);
						if (px1[0] == px2[0] && px1[3] == px2[3])
						{
							//px1[0] = 255;
							px2[1] = 255;
						}
					}


				}
			}
			radius++;
		}
	}
	
public:
	/**
		Compares letter (shapes) given in Image

		@param img - Image structure
		@return procent of similarity beetwen letters
	*/
	static double Compare(Image* img)
	{
		int columnStart1 = img->width;
		int rowStart1 = img->height;
		int columnStart2 = img->width;
		int rowStart2 = img->height;
		vector<int> x1_table = vector<int>();
		vector<int> y1_table = vector<int>();
		vector<int> x2_table = vector<int>();
		vector<int> y2_table = vector<int>();
		png_bytep row;
		png_bytep px;
		for (int i = 0; i < img->height; i++) {
			row = img->imageTable[i];
			for (int j = 0; j < img->width/2; j++) {

				px = &(row[j * 4]);
				if (px[1] == 0 && px[3] != 0)
				{
					if (i < columnStart1) columnStart1 = i;
					if (j < rowStart1) rowStart1 = j;
					x1_table.push_back(i);
					y1_table.push_back(j);
				}

				px = &(row[(j+ img->width / 2) * 4]);
				if (px[1] == 0 && px[3] != 0)
				{
					if (i < columnStart2) columnStart2 = i;
					if ((j + img->width / 2) < rowStart2) rowStart2 = (j + img->width / 2);
					x2_table.push_back(i);
					y2_table.push_back((j + img->width / 2));
				}



			}
		}
		rowStart2 += 1;
		
		int* center1 = (GetCenter(x1_table, y1_table));
		int* center2 = (GetCenter(x2_table, y2_table));

		

		
		row = img->imageTable[columnStart2];
		int cover = 0;
		int allsize = 0;
		int nullcover = 0;
		int nosize;
		if (columnStart1 > columnStart2) nosize = columnStart1;
		else nosize = columnStart2;
		
		Check(img, center1, center2);

		for (int i = 0; i < img->height - nosize; i++) {
			row = img->imageTable[i + columnStart2];
			for (int j = 0; j < (img->width / 2) - rowStart1; j++) {
				px = &(row[(j + rowStart2) * 4]);
				allsize++;
				if (px[3] == 0) nullcover++;
				else if (px[1] == 255) cover++;
			}
		}


		allsize -= nullcover;
//		cover -= nullcover;
		return (double)cover / allsize;


	}

};