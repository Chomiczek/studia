#include "MainHeader.h"

// disables warning realted to use of 
#pragma warning(disable:4244)


class Binarisation
{
private:

	/* Funkcja tworz�ca integral image obrazu (ze skryptu) */
	static void Get_integral_img(Image* image, unsigned long** integral_image)
	{
		
		unsigned long sum = 0;

		for (int y = 0; y < image->height; y++) {
			png_bytep row = image->imageTable[y];
			for (int x = 0; x < image->width; x++) {
				png_bytep px = &(row[x * 4]);
				sum += px[0];
				if (y == 0) integral_image[y][x] = sum;
				else integral_image[y][x] = integral_image[y - 1][x] + sum;
			}
		}
	}


	/* Obliczanie sumy warto�ci s�siad�w piksela [i,j] na podstawie podanego integral image*/
	static unsigned long Neighbor_Sum_With_Integral(unsigned long** integral_image,unsigned int i, unsigned int j, int ambient_radius)
	{
		return integral_image[i + ambient_radius][j + ambient_radius] + integral_image[i - ambient_radius - 1][j - ambient_radius - 1] - integral_image[i - ambient_radius - 1][j + ambient_radius] - integral_image[i + ambient_radius][j - ambient_radius - 1];
	}

	
	
public:
	

	/**
	Image binarisation using Bradley's method 

	@param img -  given Image struct
	@param singleColor - if true, only black is used and white is transparent
	@param ambient_radius - (optional) Default radius for single pixel processing 
	@param factor - (optional) Defalut factor for processing 
	@param white - (optional) Default value for color white 
	@param black - (optional) Default value for color black
	*/
	static void Bradley(Image* img, bool singleColor= false, int ambient_radius = 13,double factor = 0.5, int white = 255, int black = 0)
	{
		

		unsigned long** integral_image = new unsigned long*[img->height];
		for (int i = 0; i < img->height; i++) integral_image[i] = new unsigned long[img->width];
		Get_integral_img(img, integral_image);

		double k_factor = (1 - factor) / ((2 * ambient_radius + 1)*(2 * ambient_radius + 1));

		for (int i = 0; i < img->height; i++) {
			png_bytep row = img->imageTable[i];
			for (int j = 0; j < img->width; j++) {
				png_bytep px = &(row[j * 4]);
				if (i<ambient_radius + 1 || j<ambient_radius + 1 || i>img->height - ambient_radius - 1 || j> img->width - ambient_radius - 1)
				{
					px[0] = white;
					px[1] = white;
					px[2] = white;
					if (singleColor) px[3] = 0;
				}
				else if (px[0] < Neighbor_Sum_With_Integral(integral_image,i, j,ambient_radius)*k_factor)
				{
					{
						px[0] = black;
						px[1] = black;
						px[2] = black;
					}
				}
				else
				{
					px[0] = white;
					px[1] = white;
					px[2] = white;
					if (singleColor) px[3] = 0;
				}
				
			}
		}
	}

	/**
		Coverting given image to grayscale

		@param img - given Image struct
	*/
	static void covertToGreyscale(Image* img)
	{
		double grey_value = 0;
		for (int y = 0; y < img->height; y++) {
			png_bytep row = img->imageTable[y];
			for (int x = 0; x < img->width; x++) {
				png_bytep px = &(row[x * 4]);
				grey_value = (0.299 * px[0] + 0.587 * px[1] + 0.114 * px[2]);
				px[0] = grey_value;
				px[1] = grey_value;
				px[2] = grey_value;
				
			}
		}
	}

};