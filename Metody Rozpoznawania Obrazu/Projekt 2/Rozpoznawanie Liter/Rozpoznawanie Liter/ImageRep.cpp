#include <png.h>
#pragma once

/**
Representation of Image 
with all needed  image properties
*/
struct Image
{
	/** Width of image
	(Loaded automatically from image header)
	*/
	int width;

	/** Height of image
	(Loaded automatically from image header)
	*/
	int height;

	/** 
	Color type of image
	(Loaded automatically from image header)
	*/
	png_byte color_type;

	/**
	Depth of image
	(Loaded automatically from image header)
	*/
	png_byte bit_depth;

	/**
		Image saved as
		array of pixels

	*/
	png_bytep *imageTable;

};
