#include "MainHeader.h"

// disables warning realted to use of fopen/fclose
#pragma warning(disable:4996)

/**
	Contains all methods needed for loading 
	and saving image from file
*/
class IO {
private:

	/**
		Reads content of png img
	*/
	static bool readPNG(Image * img , FILE *fp)
	{

		png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
		if (!png) return false;

		png_infop info = png_create_info_struct(png);
		if (!info) return false;

		if (setjmp(png_jmpbuf(png))) abort();

		png_init_io(png, fp);

		png_read_info(png, info);



		img->width = png_get_image_width(png, info);
		img->height = png_get_image_height(png, info);
		img->color_type = png_get_color_type(png, info);
		img->bit_depth = png_get_bit_depth(png, info);



		if (img->bit_depth == 16)
			png_set_strip_16(png);

		if (img->color_type == PNG_COLOR_TYPE_PALETTE)
			png_set_palette_to_rgb(png);

		// PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
		if (img->color_type == PNG_COLOR_TYPE_GRAY && img->bit_depth < 8)
			png_set_expand_gray_1_2_4_to_8(png);

		if (png_get_valid(png, info, PNG_INFO_tRNS))
			png_set_tRNS_to_alpha(png);

		// These color_type don't have an alpha channel then fill it with 0xff.
		if (img->color_type == PNG_COLOR_TYPE_RGB ||
			img->color_type == PNG_COLOR_TYPE_GRAY ||
			img->color_type == PNG_COLOR_TYPE_PALETTE)
			png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

		if (img->color_type == PNG_COLOR_TYPE_GRAY ||
			img->color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
			png_set_gray_to_rgb(png);

		png_read_update_info(png, info);

		img->imageTable = (png_bytep*)malloc(sizeof(png_bytep) * img->height);
		for (int y = 0; y < img->height; y++) {
			img->imageTable[y] = (png_byte*)malloc(png_get_rowbytes(png, info));
		}
		
		png_read_image(png, img->imageTable);

		png_destroy_read_struct(&png, &info, NULL);
		png = NULL;
		info = NULL;


		return true;
	}
	

	static bool writePNG(Image * img, FILE *fp) 
	{
		
		png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
		if (!png) return false;

		png_infop info = png_create_info_struct(png);
		if (!info) return false;

		if (setjmp(png_jmpbuf(png))) abort();

		png_init_io(png, fp);

		png_set_IHDR(
			png,
			info,
			img->width, img->height,
			img->bit_depth,
			PNG_COLOR_TYPE_RGBA,
			PNG_INTERLACE_NONE,
			PNG_COMPRESSION_TYPE_DEFAULT,
			PNG_FILTER_TYPE_DEFAULT
		);
		png_write_info(png, info);

		png_write_image(png, img->imageTable);
		png_write_end(png, NULL);

		for (int y = 0; y < img->height; y++) {
			free(img->imageTable[y]);
		}
		free(img->imageTable);

		return true;
		
	}
	

public:

	/**
		Loads image with given name into given table.

		@param img - Image stuct containing image
		@param fileName - name of the file
		@return	true if file load was succesfull, false otherwise
	*/
	static bool loadImage(Image* img, std::string fileName)
	{
		
		FILE *fp = fopen(fileName.c_str(), "rb");
		
		if (fp == NULL) return false;
		bool result = readPNG(img, fp);
		fclose(fp);
		return result;

	}

	/**
	Saves image into file with given name.

	@param img - Image stuct containing image
	@param fileName - name of the file
	@return	true if save to file was succesfull, false otherwise
	*/
	static bool saveImage(Image* img, std::string fileName)
	{
		FILE *fp = fopen(fileName.c_str(), "wb");
		if (!fp) return false;
		bool result = writePNG(img, fp);
		fclose(fp);
		return result;

	}
	

};